#!/bin/sh

[ -d archive ] && exit
[ -e archive.txz ] && tar xf archive.txz && exit

wget -m -nd -np -P archive 'http://para.inria.fr/~doligez/corewar/rc/'
rm archive/robots.txt archive/index.html*
