#!/bin/sh

ls archive/*.disabled > /dev/null 2>&1 && exit

cd archive

# fix some syntax errors
sed -i '5s/for/_for_/' BagofTricks.txt
sed -i 's/ 0/ 0x/g ; s/./,/28' C10n642.txt
sed -zi 's/,\n/, / ; s/e \n/e / ; s/H\t/H\n/' DATMurderer.txt
sed -zi 's/a\n/a /' EBOLAstrain2.1.txt
sed -i '6s/[ac]/0/g ; 8s/[cd]/0/g' Emerald\[2\].txt
sed -i 'y/q/0/' Failure.txt
sed -i 'y/S/s/' FIV1.txt FIV1\[a\].txt
sed -zi 's/rse\n/rse / ; s/ve\n/ve /' Glasswalker.txt
sed -i -e '3i FOR 0' -e '7a ROF' invicta.txt
sed -i 's/\t/,\t/3 ; 19,20s/ /,/' JR-26-8192.txt
sed -i '11s/for/_for_/' JudgementDay.txt
sed -i 's/S$/s/' JulietStormSpiral.txt
sed -i '12!y/sp/SP/' Killer2.txt
sed -i '17s/O/xO/' kirin2.txt
sed -i '14s/a/1/ ; 15s/b/2/ ; 16,17s/c/3/1 ; 18s/d/5/' Knowyourenemy.txt
sed -i 's/ \././' LikeRabbits!.txt
sed -i 's/CLOOP/cloop/' Lostboy.txt
sed -i '131s/z/\&z/g' Maybe.txt
sed -zi 's/=\n/= / ; s/R\n/R /2' Minev0.3.txt
sed -zi 's/e \n/e /' MIRV.txt
sed -i '21s/2 /2,/' Mr.Beef.txt
sed -i 's/SPL,/SPL /' MuleDNA\[short\].txt
sed -i 's/\.\././' Newtv0.2.txt
sed -i 's/\.    /./' NotVeryPretty2.0.txt
sed -i 'y/*/;/' people.txt
sed -i 's/\<div\>/_div/' Promotion.txt
sed -i '31s/,/ /' QuarterII.txt
sed -i '35s/,//' QuickSandII.txt
sed -i 's/i$/0/' Reclaiming.txt
sed -i '100d' Rhino.txt
sed -i '6s/T/TANCE/' runandhit.txt
sed -zi 's/d \n/d /' ShellSort.txt
sed -zi 's/3\n/3 /' Sortidator.txt
sed -i 's/start/START/' Spook.txt
sed -zi 's/rt\n/rt /' TottiatwaE.txt
sed -i '497s/,/ /' UMACCP.txt
sed -i '35s/for/_for_/' v15.txt
sed -i 's/u$/ut/ ; 98s/,//' viralOS.txt
sed -i 's/rt1/rt/' VOTER.txt
sed -i 'y/*/;/' W.H.Imp.txt

# remove erroneous programs
cut -d\  -f1 <<- EOF | xargs rename 's/$/.disabled/'
	Blur.txt                         missing label definition
	BubblySort1b.txt                 subroutine using ITEMS and ITEMLIST
	cerebus.txt                      unknown instruction DJZ
	Chaser.txt                       unknown instruction JSI
	ClearSightedv1.txt               invalid syntax
	confetti.txt                     missing label definition
	Consort.txt                      subroutine using ITEMS and ITEMLIST
	Dagger.txt                       invalid syntax
	E-Call.txt                       missing label definition
	fang.txt                         invalid syntax
	fastworm.txt                     invalid syntax
	GCD.txt                          subroutine using quit
	geminicannon.txt                 invalid syntax
	GEMINI.txt                       invalid syntax
	Ghost.txt                        invalid syntax + multiply defined label
	HopefullyIndestructiblev1.0.txt  subroutine using gate
	impcannon[1].txt                 invalid syntax
	Memory1.0.txt                    unknown instruction JMG
	muncher.txt                      invalid syntax
	nameless-WS.txt                  missing label definition
	Nematodev1.3e+.txt               missing label definition
	OGRE[xx].txt                     invalid syntax
	P-spacedemo.txt                  invalid syntax: "for 0 ... for/rof ... rof", "rof expression"
	ParticleSorter.txt               subroutine using ITEMS and ITEMLIST
	Pink.txt                         invalid syntax
	PSortv1.0.txt                    subroutine using ITEMS and ITEMLIST
	Q^2.txt                          missing label definition
	round1.txt                       missing label definition
	Seasidesort.txt                  subroutine using ITEMS and ITEMLIST
	SMARTFRIAR.txt                   invalid syntax + unknown instructions
	Sortidator.txt                   subroutine using ITEMS and ITEMLIST
	Sortv1.5.txt                     subroutine using ITEMS and ITEMLIST
	Swing.txt                        missing label definition
	Vampire[1].txt                   invalid syntax
	Virus[JMMM].txt                  unknown instruction BSS
	WORM.txt                         invalid syntax
EOF

# separate supported and unsupported programs by name change
while read FILE; do
	NAME=$(basename $FILE .txt)
	mv $NAME.txt $NAME.unsupported
	awk -v IGNORECASE=1 '
	/^[ \t]*(;|\w*[ \t]*\<equ\>)/ {
		print
		next
	}
	{ CODE = CODE $0 "\n" }
	END { print CODE }
	' $NAME.unsupported > $NAME.supported
done <<- EOF
	Aleph0.txt
	Alladin'sCave.txt
	AlmosttheLastOne.txt
	Al'sCave.txt
	Altruist23.txt
	Bigagain.txt
	BigI.F.F.S..txt
	Big.txt
	BlackBoxv1.txt
	Blizzard.txt
	CannonJr.txt
	Cinnamon.txt
	Cocktail.txt
	Combatra.txt
	Double.txt
	Evoltmp88[2].txt
	Evoltmp88.txt
	FleetfootinaRose-Garden.txt
	Fscan.txt
	GEMINICANNON.txt
	Glasswalker.txt
	HailStorm.txt
	Harquebus.txt
	HiHoSilver1.txt
	Holdstillaminute...Thanks.txt
	ImpCraze91.43.txt
	ivscan6[1].txt
	ivscan6[2].txt
	Jaguar.txt
	KATv5.txt
	KisstheFishBabe!.txt
	Knightmare.txt
	LPandFeelin'Fine.txt
	meld.txt
	ModusVivendi320.txt
	ModusVivendi510.txt
	Mutual-repairingpair.txt
	NCC-1701-A.txt
	Nova-A.1b.txt
	P^2.txt
	PaperKiller2.1.txt
	Pattel'sVirus.txt
	Psst2.0.txt
	QuickThinking.txt
	Randomizer.txt
	rock.txt
	Settete.txt
	Silccon.txt
	Sissy.txt
	Small.txt
	SonicBoom1.30.txt
	Spider1.1.txt
	Splash1.txt
	SwitchBlade.txt
	test[JKW].txt
	ThreeMusketeers.txt
	VariationG-1.txt
EOF
