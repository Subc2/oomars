#include <cmath>

#include "corewar.hh"

using namespace corewar;

typedef CoordinatesConverter::Coordinates Coordinates;

namespace {
	const std::map<const std::string, Operation> Operations = {
		{"ADD", Operation::ADD},
		{"CMP", Operation::CMP},
		{"DAT", Operation::DAT},
		{"DIV", Operation::DIV},
		{"DJN", Operation::DJN},
		{"JMN", Operation::JMN},
		{"JMP", Operation::JMP},
		{"JMZ", Operation::JMZ},
		{"LDP", Operation::LDP},
		{"MOD", Operation::MOD},
		{"MOV", Operation::MOV},
		{"MUL", Operation::MUL},
		{"NOP", Operation::NOP},
		{"SEQ", Operation::SEQ},
		{"SLT", Operation::SLT},
		{"SNE", Operation::SNE},
		{"SPL", Operation::SPL},
		{"STP", Operation::STP},
		{"SUB", Operation::SUB}
	};

	const std::map<const std::string, Modifier> Modifiers = {
		{".A",  Modifier::A},
		{".AB", Modifier::AB},
		{".B",  Modifier::B},
		{".BA", Modifier::BA},
		{".F",  Modifier::F},
		{".I",  Modifier::I},
		{".X",  Modifier::X}
	};

	const std::map<const std::string, AddrMode> AddrModes = {
		{"",  AddrMode::DIR},
		{"#", AddrMode::IMM},
		{"$", AddrMode::DIR},
		{"*", AddrMode::A_IND},
		{"<", AddrMode::B_IND_PRE},
		{">", AddrMode::B_IND_POS},
		{"@", AddrMode::B_IND},
		{"{", AddrMode::A_IND_PRE},
		{"}", AddrMode::A_IND_POS}
	};

	std::shared_ptr<const Hill> makeSharedHill(std::initializer_list<std::pair<const std::string, int>> &&init) {
		return std::make_shared<const Hill>(
				std::initializer_list<std::pair<const std::string, int>>(std::move(init)));
	}

	void setDefaultModifier(Opcode &opcode) {
		switch (opcode.operation) {
			case Operation::DAT:
			case Operation::NOP:
				opcode.modifier = Modifier::F;
				break;
			case Operation::MOV:
			case Operation::SEQ:
			case Operation::SNE:
			case Operation::CMP:
				if (opcode.addrModeA == AddrMode::IMM)
					opcode.modifier = Modifier::AB;
				else if (opcode.addrModeB == AddrMode::IMM)
					opcode.modifier = Modifier::B;
				else
					opcode.modifier = Modifier::I;
				break;
			case Operation::ADD:
			case Operation::SUB:
			case Operation::MUL:
			case Operation::DIV:
			case Operation::MOD:
				if (opcode.addrModeA == AddrMode::IMM)
					opcode.modifier = Modifier::AB;
				else if (opcode.addrModeB == AddrMode::IMM)
					opcode.modifier = Modifier::B;
				else
					opcode.modifier = Modifier::F;
				break;
			case Operation::SLT:
			case Operation::LDP:
			case Operation::STP:
				opcode.modifier = opcode.addrModeA == AddrMode::IMM
						? Modifier::AB
						: Modifier::B;
				break;
			case Operation::JMP:
			case Operation::JMZ:
			case Operation::JMN:
			case Operation::DJN:
			case Operation::SPL:
				opcode.modifier = Modifier::B;
				break;
		}
	}
}

namespace corewar {
	const int Version = 130;

	const std::array<std::shared_ptr<const Hill>, 5> Hills = {
		makeSharedHill({  // ICWS'94 Standard Hill (redcode-94)
			{"CORESIZE", 8000},
			{"MAXCYCLES", 80000},
			{"MAXLENGTH", 100},
			{"MINDISTANCE", 100},
			{"MAXPROCESSES", 8000},
			{"PSPACESIZE", 500},
			{"VERSION", Version},
			{"WARRIORS", 2}
		}), makeSharedHill({  // ICWS'94 NOP Hill (redcode-94nop)
			{"CORESIZE", 8000},
			{"MAXCYCLES", 80000},
			{"MAXLENGTH", 100},
			{"MINDISTANCE", 100},
			{"MAXPROCESSES", 8000},
			{"PSPACESIZE", 0},
			{"VERSION", Version},
			{"WARRIORS", 2}
		}), makeSharedHill({  // ICWS'94 Experimental Hill (redcode-94x)
			{"CORESIZE", 55440},
			{"MAXCYCLES", 500000},
			{"MAXLENGTH", 200},
			{"MINDISTANCE", 200},
			{"MAXPROCESSES", 10000},
			{"PSPACESIZE", 3465},
			{"VERSION", Version},
			{"WARRIORS", 2}
		}), makeSharedHill({  // ICWS'94 Multiwarrior Hill (redcode-94m)
			{"CORESIZE", 8000},
			{"MAXCYCLES", 80000},
			{"MAXLENGTH", 100},
			{"MINDISTANCE", 100},
			{"MAXPROCESSES", 8000},
			{"PSPACESIZE", 500},
			{"VERSION", Version},
			{"WARRIORS", 10}
		}), makeSharedHill({  // ICWS'94 Multiwarrior X Hill (redcode-94xm)
			{"CORESIZE", 55440},
			{"MAXCYCLES", 500000},
			{"MAXLENGTH", 200},
			{"MINDISTANCE", 200},
			{"MAXPROCESSES", 10000},
			{"PSPACESIZE", 3465},
			{"VERSION", Version},
			{"WARRIORS", 10}
		})
	};

	Opcode::Opcode()
			: operation(Operation::DAT),
			  modifier(Modifier::F),
			  addrModeA(AddrMode::DIR),
			  addrModeB(AddrMode::DIR) {}

	Opcode::Opcode(const std::string &Op, const std::string &AddrA) try
			: operation(Operations.at(Op.substr(0, 3))),
			  addrModeA(AddrModes.at(AddrA)),
			  addrModeB(AddrMode::DIR) {
		if (Op[3] == '.')
			modifier = Modifiers.at(Op.substr(3));
		else
			setDefaultModifier(*this);
	} catch (const std::out_of_range &) {
		if (!AddrModes.count(AddrA))
			throw Exception("invalid addressing mode \"" + AddrA + "\"");
		throw Exception("invalid operation \"" + Op + "\"");
	}

	Opcode::Opcode(const std::string &Op, const std::string &AddrA, const std::string &AddrB) try
			: operation(Operations.at(Op.substr(0, 3))),
			  addrModeA(AddrModes.at(AddrA)),
			  addrModeB(AddrModes.at(AddrB)) {
		if (Op[3] == '.')
			modifier = Modifiers.at(Op.substr(3));
		else
			setDefaultModifier(*this);
	} catch (const std::out_of_range &) {
		for (const auto &Addr : {AddrA, AddrB})
			if (!AddrModes.count(Addr))
				throw Exception("invalid addressing mode \"" + Addr + "\"");
		throw Exception("invalid operation \"" + Op + "\"");
	}

	Opcode::Opcode(Operation op, Modifier mod, AddrMode addrA, AddrMode addrB)
			: operation(op), modifier(mod), addrModeA(addrA), addrModeB(addrB) {}

	bool Opcode::operator!=(const Opcode &Other) const {
		return operation != Other.operation
				|| modifier != Other.modifier
				|| addrModeA != Other.addrModeA
				|| addrModeB != Other.addrModeB;
	}

	bool Opcode::operator==(const Opcode &Other) const {
		return !operator!=(Other);
	}


	Instruction::Instruction() : fieldA(0), fieldB(0) {}

	Instruction::Instruction(Opcode opcode, int a)
			: opcode(opcode), fieldA(a), fieldB(0) {
		if (opcode.operation == Operation::DAT) {
			std::swap(opcode.addrModeA, opcode.addrModeB);
			std::swap(fieldA, fieldB);
		}
	}

	Instruction::Instruction(Opcode opcode, int a, int b)
			: opcode(opcode), fieldA(a), fieldB(b) {}

	bool Instruction::operator!=(const Instruction &Other) const {
		return opcode != Other.opcode
				|| fieldA != Other.fieldA
				|| fieldB != Other.fieldB;
	}

	bool Instruction::operator==(const Instruction &Other) const {
		return !operator!=(Other);
	}


	Warrior::Warrior() : origin(0), pin(-1) {}


	Coordinates::operator int() const {
		return offset;
	}

	CoordinatesConverter::CoordinatesConverter(int size) : size(size) {
		auto set_dim = [&](int h) {
			return size % h ? false : (height = h, width = size / h, true);
		};
		int size_sqrt = sqrt(size);
		for (int i = size_sqrt / sqrt(2); i <= size_sqrt; ++i)
			if (set_dim(i))
				return;
		for (int i = size_sqrt / sqrt(2) - 1;; --i)
			if (set_dim(i))
				return;
	}

	CoordinatesConverter::CoordinatesConverter(int width, int height)
			: size(width * height), width(width), height(height) {}

	Coordinates CoordinatesConverter::operator()(int offset) const {
		return {offset, offset % width, offset / width};
	}

	Coordinates CoordinatesConverter::operator()(int x, int y) const {
		return {y * width + x, x, y};
	}


	Exception::Exception(const std::string &what_arg)
			: std::runtime_error(what_arg) {}

	ArenaError::ArenaError(const std::string &what_arg)
			: Exception(what_arg) {}

	AssemblerError::AssemblerError(const std::string &what_arg)
			: Exception(what_arg) {}

	AssertionError::AssertionError(const std::string &what_arg)
			: Exception(what_arg) {}
}
