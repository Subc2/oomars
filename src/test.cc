#include <fstream>

#include "parser/RedcodeParser.hh"

using parser::RedcodeParser;

int tryToParse(RedcodeParser &parser, const char *filename = nullptr) {
	try {
		if (parser.parse())
			throw corewar::AssemblerError("syntax error");
		parser.assemble();
	} catch (const corewar::AssertionError &) {
	} catch (const std::exception &E) {
		std::cerr << (filename ? : "-") << ": " << E.what() << '\n';
		return 1;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	if (argc == 2) {
		std::ifstream in(argv[1]);
		RedcodeParser parser(in);
		return tryToParse(parser, argv[1]);
	}

	RedcodeParser parser;
	return tryToParse(parser);
}
