#pragma once

#include <QErrorMessage>
#include <QFormLayout>
#include <QLabel>
#include <QListWidget>
#include <QMainWindow>
#include <QStackedWidget>
#include <QTableWidget>
#include <QTextStream>

#include "../corewar.hh"

namespace ui {
	class QtUI : public QMainWindow {
		public:
			static QRegExpValidator *name_validator;
			static QRegExpValidator *number_validator;

			QtUI(const std::list<std::string> &);

		protected:
			void keyPressEvent(QKeyEvent *);
			void timerEvent(QTimerEvent *);

		private:
			void addFileToList(const QString &, QListWidgetItem * = nullptr);
			void loadBaseHill(int);
			void addVariableToHill(const QString &, const QString &);
			void addLastRowToHill();
			void setUpArenaInterface();
			void selectArenaCell(QPoint);
			void updatedArenaOffset(int);
			void updatedPspaceCell();
			void unlockWidgets(bool);
			void simulateTick();
			void shiftInterval(bool);
			QPixmap tile(int);

			QTextStream cerr;
			QErrorMessage *error;
			QDialog *help;

			QStackedWidget *main_widget;
			QListWidget *file_list;
			QTableWidget *hill_variables;
			QHBoxLayout *warriors_info;
			QLabel *arena_label, *pspace_label, *step_label;
			QFormLayout *instruction_edit;
			QGridLayout *processes_info;
			QFormLayout *pspace_edit;
			QPushButton *next_round;

			class ArenaGraphicsView *arena_view;
			QGraphicsScene *arena_scene;
			QVector<QGraphicsPixmapItem *> arena_items;
			QVector<QPixmap> tiles;
			QMap<int, QPixmap> cached_pixmaps;

			std::shared_ptr<corewar::ArenaInterface> arena;
			corewar::CoordinatesConverter cc;
			QPoint selected;
			QPair<uint8_t, int> selected_pspace;

			QVector<QString> warriors_names;
			int timer, interval;
			bool debug;

		friend ArenaGraphicsView;
	};
}
