#pragma once

#include <QGraphicsScene>

#include "../corewar.hh"

namespace ui {
	class SimpleQtUI : public QGraphicsScene {
		public:
			SimpleQtUI(std::shared_ptr<corewar::ArenaInterface>, QPixmap *);
			~SimpleQtUI();

		protected:
			void keyPressEvent(QKeyEvent *);
			void timerEvent(QTimerEvent *);
			void simulateTick();
			void gameOver();
			const QPixmap &tile(int) const;

		private:
			std::shared_ptr<corewar::ArenaInterface> arena;
			corewar::CoordinatesConverter cc;
			QPixmap *tileset, tiles[9];
			QGraphicsPixmapItem ***graphics_items;
			int timer;
			bool gameover;
			int delay;
	};
}
