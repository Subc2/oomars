#include <QApplication>
#include <QCheckBox>
#include <QComboBox>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGroupBox>
#include <QHeaderView>
#include <QKeyEvent>
#include <QLineEdit>
#include <QMap>
#include <QMessageBox>
#include <QPushButton>
#include <QSpinBox>
#include <QStringBuilder>
#include <QStyledItemDelegate>

#include "QtUI.hh"
#include "../arena.hh"
#include "../assembler.hh"

using corewar::Opcode;
using corewar::Instruction;
using corewar::CommentType;
using corewar::HillType;
using corewar::Tile;

namespace {
	enum class TileOffset {
		SELECTED = 0, TILES = 1, MULTIPLE = 5, SINGLE = 6, MODIFIED = 16
	};

	struct NameDelegate : public QStyledItemDelegate {
		NameDelegate(QObject *parent = Q_NULLPTR) : QStyledItemDelegate(parent) {}

		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const {
			QLineEdit *editor = new QLineEdit(parent);
			editor->setValidator(ui::QtUI::name_validator);
			return editor;
		}
	};

	struct NumberDelegate : public QStyledItemDelegate {
		NumberDelegate(QObject *parent = Q_NULLPTR) : QStyledItemDelegate(parent) {}

		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &) const {
			QLineEdit *editor = new QLineEdit(parent);
			editor->setValidator(ui::QtUI::number_validator);
			return editor;
		}
	};

	template<typename T>
	QList<T *> findLayoutChildren(QLayout *layout) {
		QObject *(QLayoutItem::*func)() = std::is_base_of<QWidget, T>()
				? reinterpret_cast<QObject *(QLayoutItem::*)()>(&QLayoutItem::widget)
				: reinterpret_cast<QObject *(QLayoutItem::*)()>(&QLayoutItem::layout);
		QList<T *> list;
		QLayoutItem *child;
		for (int i = 0; (child = layout->itemAt(i)) != nullptr; ++i) {
			T *item = qobject_cast<T *>((child->*func)());
			if (item)
				list.append(item);
		}
		return list;
	}

	void boundSpinBox(QSpinBox *spinbox, int modulo, bool bound) {
		if (bound) {
			int value = spinbox->value() % modulo;
			spinbox->setRange(0, modulo - 1);
			spinbox->setValue(value + (value < 0) * modulo);
		} else {
			int value = spinbox->value();
			spinbox->setRange(-modulo, modulo);
			spinbox->setValue(value - (value > modulo / 2) * modulo);
		}
	}

	QIcon getIcon(const QString &Filename) {
		return QIcon::fromTheme(
				Filename.section('/', -1).section('.', 0, -2),
				QIcon(":/" + Filename));
	}
}

namespace ui {
	struct ArenaGraphicsView : public QGraphicsView {
		QtUI *ui;

		ArenaGraphicsView(QtUI *parent) : QGraphicsView(parent), ui(parent) {}

		void mouseMoveEvent(QMouseEvent *event) {
			QPoint P = mapToScene(event->pos()).toPoint();
			ui->selectArenaCell(QPoint(P.x() / 8, P.y() / 8));
		}

		void mousePressEvent(QMouseEvent *event) {
			mouseMoveEvent(event);
		}
	};

	QRegExpValidator *QtUI::name_validator = nullptr;
	QRegExpValidator *QtUI::number_validator = nullptr;

	QtUI::QtUI(const std::list<std::string> &WarriorsFiles)
			: cerr(stderr), error(new QErrorMessage(this)), help(nullptr),
			  arena_view(new ArenaGraphicsView(this)), arena_scene(nullptr),
			  cc(0, 0), timer(0), interval(0x100), debug(false) {
		{
			QPixmap tileset(":/tileset.png");
			if (tileset.isNull()) {
				cerr << "Cannot open tileset \"tileset.png\"." << endl;
				exit(EXIT_FAILURE);
			}
			if (tileset.size() != QSize(26 * 8, 8)) {
				cerr << "Tileset \"tileset.png\" has invalid size." << endl;
				exit(EXIT_FAILURE);
			}
			tiles.reserve(26);
			for (int i = 0; i < 26; ++i)
				tiles.append(tileset.copy(i * 8, 0, 8, 8));
		}

		name_validator = new QRegExpValidator(
				QRegExp("[a-z_][a-z0-9_]*", Qt::CaseInsensitive), this);
		number_validator = new QRegExpValidator(
				QRegExp("[0-7]+|[1-9]\\d*|0x[0-9a-f]+", Qt::CaseInsensitive), this);

		// set up file_list
		file_list = new QListWidget;
		for (const auto &Filename : WarriorsFiles)
			addFileToList(Filename.c_str());

		// set up hill_variables
		hill_variables = new QTableWidget(0, 3);
		hill_variables->horizontalHeader()->hide();
		hill_variables->horizontalHeader()->setStretchLastSection(true);
		hill_variables->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
		hill_variables->verticalHeader()->hide();
		hill_variables->setItemDelegateForColumn(0, new NameDelegate(this));
		hill_variables->setItemDelegateForColumn(1, new NumberDelegate(this));
		loadBaseHill(static_cast<int>(HillType::R94));

		// set up load_page and load_page_layout
		QWidget *load_page = new QWidget;
		QGridLayout *load_page_layout = new QGridLayout(load_page);
		load_page_layout->addWidget(new QLabel("warriors:"), 0, 0);
		load_page_layout->addWidget(new QLabel("choose hill:"), 0, 1);
		load_page_layout->addWidget(file_list, 1, 0, 2, 1);
		{
			QComboBox *hills_list = new QComboBox;
			hills_list->addItems({"R94", "R94NOP", "R94X", "R94M", "R94XM"});
			connect(
					hills_list,
					static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
					[this](int index) { loadBaseHill(index); });
			load_page_layout->addWidget(hills_list, 1, 1);
		}
		load_page_layout->addWidget(hill_variables, 2, 1);
		{
			QPushButton *button = new QPushButton("add warriors");
			connect(button, &QPushButton::clicked, [this](bool) {
				for (const QString &Filename : QFileDialog::getOpenFileNames(this))
					addFileToList(Filename);
			});
			load_page_layout->addWidget(button, 3, 0);
		}
		{
			QPushButton *button = new QPushButton("load arena");
			connect(button, &QPushButton::clicked, [this](bool) {
				setUpArenaInterface();
			});
			load_page_layout->addWidget(button, 3, 1);
		}

		// set up arena_page and arena_page_layout
		QWidget *arena_page = new QWidget;
		QVBoxLayout *arena_page_layout = new QVBoxLayout(arena_page);
		{
			QHBoxLayout *layout = new QHBoxLayout;
			{
				QPushButton *button = new QPushButton(getIcon("go-previous.svg"), "back");
				connect(button, &QPushButton::clicked, [this](bool) {
					killTimer(std::exchange(timer, 0));
					arena = nullptr;
					main_widget->setCurrentIndex(0);
				});
				layout->addWidget(button);
			}
			layout->addStretch();
			{
				QPushButton *button = new QPushButton(getIcon("help-about.svg"), "help");
				connect(button, &QPushButton::clicked, [this](bool) {
					if (help) {
						help->raise();
						help->activateWindow();
						return;
					}
					help = new QDialog(this);
					QVBoxLayout *help_layout = new QVBoxLayout(help);
					{
						QGroupBox *shortcuts = new QGroupBox("shortcuts");
						shortcuts->setFlat(true);
						QFormLayout *shortcuts_layout = new QFormLayout(shortcuts);
						shortcuts_layout->setLabelAlignment(Qt::AlignLeft);
						for (const auto &P : std::initializer_list<QPair<QString, QString>>{
								{"Q", "quit application"},
								{"H", "move left"},
								{"K", "move up"},
								{"L", "move right"},
								{"J", "move down"},
								{"M", "toggle manual control"},
								{"Space", "next cycle"},
								{"+", "increase speed"},
								{"-", "decrease speed"}
						})
							shortcuts_layout->addRow(P.first, new QLabel(P.second));
						help_layout->addWidget(shortcuts);
					}
					{
						QGroupBox *icons = new QGroupBox("icons");
						icons->setFlat(true);
						QFormLayout *icons_layout = new QFormLayout(icons);
						for (const auto &P : std::initializer_list<QPair<int, QString>>{
								{int(TileOffset::SELECTED), "selected"},
								{int(TileOffset::MULTIPLE), "occupied by multiple warriors"},
								{int(TileOffset::SINGLE), "occupied by 1st warrior"},
								{int(TileOffset::MODIFIED), "last modified by 1st warrior"},
								{-1, "..."},
								{int(TileOffset::TILES) + int(Tile::CODE), "ordinary instruction"},
								{int(TileOffset::TILES) + int(Tile::DATA), "DAT instruction"},
								{int(TileOffset::TILES) + int(Tile::JUMP), "jump instruction"},
								{int(TileOffset::TILES) + int(Tile::SPLIT), "SPL instruction"}
						})
							if (P.first < 0)
								icons_layout->addRow(new QLabel(P.second));
							else {
								QLabel *label = new QLabel;
								label->setPixmap(tiles[P.first]);
								icons_layout->addRow(label, new QLabel(P.second));
							}
						help_layout->addWidget(icons);
					}
					help->show();
					help->raise();
					help->activateWindow();
					connect(help, &QDialog::finished, [this](int) { help = nullptr; });
				});
				layout->addWidget(button);
			}
			arena_page_layout->addLayout(layout);
		}
		arena_page_layout->addLayout(warriors_info = new QHBoxLayout);
		{
			arena_page_layout->addWidget(arena_view);
			arena_page_layout->setStretchFactor(arena_view, 9);
		}
		{
			QGridLayout *bottom = new QGridLayout;
			for (int i : {0, 1, 2, 3})
				bottom->setColumnStretch(i, 9);
			{
				bottom->addWidget(arena_label = new QLabel, 0, 0, 1, 2);
				arena_label->setAlignment(Qt::AlignHCenter);
			}
			bottom->addWidget(pspace_label = new QLabel, 0, 2);
			{
				QCheckBox *checkbox = new QCheckBox("debug");
				checkbox->setCheckState(debug ? Qt::Checked : Qt::Unchecked);
				connect(checkbox, &QCheckBox::stateChanged, [this](int state) {
					if (debug != (state == Qt::Checked)) {
						debug = !debug;
						if (!timer)
							unlockWidgets(debug);
					}
				});
				bottom->addWidget(checkbox, 0, 3);
			}
			bottom->addWidget(new QLabel("instruction:"), 1, 0);
			bottom->addWidget(new QLabel("number of processes:"), 1, 1);
			{
				pspace_edit = new QFormLayout;
				pspace_edit->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
				{
					QComboBox *warrior = new QComboBox;
					connect(
							warrior,
							static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
							[this](int index) {
								selected_pspace.first = index + 1;
								updatedPspaceCell();
							});
					QSpinBox *offset = new QSpinBox, *value = new QSpinBox;
					connect(
							offset,
							static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
							[this](int i) {
								if ((i %= arena->pspaceSize()) < 0)
									i += arena->pspaceSize();
								selected_pspace.second = i;
								updatedPspaceCell();
							});
					value->setButtonSymbols(QAbstractSpinBox::PlusMinus);
					value->setEnabled(debug);
					connect(
							value,
							static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
							[this](int i) {
								// ArenaInterface should handle i < 0 case
								arena->setPspaceAt(selected_pspace.first, selected_pspace.second, i);
							});
					QCheckBox *pspacesize = new QCheckBox("show modulo PSPACESIZE"),
							*coresize = new QCheckBox("show modulo CORESIZE");
					connect(pspacesize, &QCheckBox::stateChanged, [this, offset](int state) {
						bool bound = state == Qt::Checked;
						boundSpinBox(offset, bound ? arena->pspaceSize() : arena->size(), bound);
					});
					connect(coresize, &QCheckBox::stateChanged, [this, value](int state) {
						boundSpinBox(value, arena->size(), state == Qt::Checked);
					});
					pspace_edit->addRow(warrior);
					pspace_edit->addRow("offset:", offset);
					pspace_edit->addRow("value:", value);
					pspace_edit->addRow(pspacesize);
					pspace_edit->addRow(coresize);
				}
				QHBoxLayout *layout = new QHBoxLayout;
				layout->addLayout(pspace_edit);
				layout->addStretch();
				bottom->addLayout(layout, 1, 2, 2, 1);
			}
			bottom->addWidget(step_label = new QLabel, 1, 3);
			{
				instruction_edit = new QFormLayout;
				instruction_edit->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
				auto setter = [this](auto ptr) {
					return [this, ptr](int index) {
						int offset = cc(selected.x(), selected.y());
						Instruction instruction = arena->getCoreAt(offset);
						typedef std::decay_t<decltype(instruction.opcode.*ptr)> T;
						instruction.opcode.*ptr = static_cast<T>(index);
						arena->setCoreAt(offset, instruction);
						if (std::is_same<T, corewar::Operation>())
							updatedArenaOffset(offset);
					};
				};
				{
					QComboBox *instruction = new QComboBox, *modifier = new QComboBox;
					instruction->addItems({
							"DAT", "MOV", "ADD", "SUB", "MUL", "DIV", "MOD", "JMP", "JMZ", "JMN",
							"DJN", "SPL", "CMP", "SEQ", "SNE", "SLT", "LDP", "STP", "NOP"
					});
					instruction->setEnabled(debug);
					connect(
							instruction,
							static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
							setter(&Opcode::operation));
					modifier->addItems({"A", "B", "AB", "BA", "F", "X", "I"});
					modifier->setEnabled(debug);
					connect(
							modifier,
							static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
							setter(&Opcode::modifier));
					instruction_edit->addRow(instruction, modifier);
				}
				for (const auto &Ptrs : {
						qMakePair(&Opcode::addrModeA, &Instruction::fieldA),
						qMakePair(&Opcode::addrModeB, &Instruction::fieldB)
				}) {
					QComboBox *addrmode = new QComboBox;
					addrmode->addItems({"#", "$", "*", "@", "{", "<", "}", ">"});
					addrmode->setEnabled(debug);
					connect(
							addrmode,
							static_cast<void (QComboBox::*)(int)>(&QComboBox::activated),
							setter(Ptrs.first));
					QSpinBox *field = new QSpinBox;
					field->setButtonSymbols(QAbstractSpinBox::PlusMinus);
					field->setEnabled(debug);
					connect(
							field,
							static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
							[this, ptr = Ptrs.second](int i) {
								if (!debug)
									return;
								int offset = cc(selected.x(), selected.y());
								Instruction instruction = arena->getCoreAt(offset);
								// ArenaInterface should handle i < 0 case
								instruction.*ptr = i;
								arena->setCoreAt(offset, instruction);
							});
					instruction_edit->addRow(addrmode, field);
				}
				{
					QCheckBox *checkbox = new QCheckBox("show modulo CORESIZE");
					connect(checkbox, &QCheckBox::stateChanged, [this](int state) {
						for (const auto &Spinbox : findLayoutChildren<QSpinBox>(instruction_edit))
							boundSpinBox(Spinbox, arena->size(), state == Qt::Checked);
					});
					instruction_edit->addRow(checkbox);
				}
				QHBoxLayout *layout = new QHBoxLayout;
				layout->addLayout(instruction_edit);
				layout->addStretch();
				bottom->addLayout(layout, 2, 0);
			}
			{
				bottom->addLayout(processes_info = new QGridLayout, 2, 1);
				processes_info->setOriginCorner(Qt::BottomLeftCorner);
				processes_info->setRowStretch(0, 1);
				processes_info->setColumnStretch(3, 1);
			}
			{
				QGridLayout *layout = new QGridLayout;
				for (const auto &P : std::initializer_list<QPair<QString, std::function<void (bool)>>>{
						{"media-playback-start.svg", [this](bool) {
							timer = timer ? : startTimer(interval);
							unlockWidgets(false); }},
						{"media-playback-pause.svg", [this](bool) {
							killTimer(std::exchange(timer, 0));
							unlockWidgets(debug); }},
						{"media-skip-forward.svg", [this](bool) {
							simulateTick(); }},
						{"list-add.svg", [this](bool) {
							shiftInterval(false); }},
						{"filename-dash-amarok.svg", [this](bool) {
							shiftInterval(true); }}
				}) {
					QPushButton *button = new QPushButton(getIcon(P.first), "");
					button->setFlat(true);
					connect(button, &QPushButton::clicked, P.second);
					// QGridLayout::columnCount() starts from 1
					layout->addWidget(button, 0, layout->columnCount());
				}
				{
					next_round = new QPushButton("next round");
					connect(next_round, &QPushButton::clicked, [this](bool) {
						arena->nextRound();
						uint8_t id = 1;
						for (const auto &Layout : findLayoutChildren<QFormLayout>(warriors_info)) {
							auto get_label = [=](int row) {
								return qobject_cast<QLabel *>(
										Layout->itemAt(row, QFormLayout::FieldRole)->widget());
							};
							if (arena->getPspaceAt(id, 0) != 0)
								get_label(3)->setText(QString::number(get_label(3)->text().toInt() + 1));
							get_label(1)->setText("1");
							get_label(2)->setText(QString::number(arena->queueFront(id++)));
						}
						for (int i = 0; i < arena->size(); ++i)
							updatedArenaOffset(i);
						updatedPspaceCell();
						next_round->setEnabled(false);
					});
					layout->addWidget(next_round, 1, 1, 1, 5);
				}
				layout->setRowStretch(2, 1);
				layout->setColumnStretch(layout->columnCount(), 1);
				bottom->addLayout(layout, 2, 3);
			}
			arena_page_layout->addLayout(bottom);
		}

		// set up main_widget
		main_widget = new QStackedWidget;
		main_widget->addWidget(load_page);
		main_widget->addWidget(arena_page);
		setCentralWidget(main_widget);
	}

	void QtUI::keyPressEvent(QKeyEvent *event) {
		if (!arena)
			return;
		switch (event->key()) {
			case Qt::Key_Q:
				qApp->quit();
				break;
			case Qt::Key_H:
			case Qt::Key_Left:
				selectArenaCell(selected + QPoint(-1, 0));
				break;
			case Qt::Key_K:
			case Qt::Key_Up:
				selectArenaCell(selected + QPoint(0, -1));
				break;
			case Qt::Key_L:
			case Qt::Key_Right:
				selectArenaCell(selected + QPoint(1, 0));
				break;
			case Qt::Key_J:
			case Qt::Key_Down:
				selectArenaCell(selected + QPoint(0, 1));
				break;
			case Qt::Key_M:
				if (timer) {
					killTimer(std::exchange(timer, 0));
					unlockWidgets(debug);
				} else {
					timer = startTimer(interval);
					unlockWidgets(false);
				}
				break;
			case Qt::Key_Space:
				simulateTick();
				break;
			case Qt::Key_Plus:
				shiftInterval(false);
				break;
			case Qt::Key_Minus:
				shiftInterval(true);
				break;
		}
	}

	void QtUI::timerEvent(QTimerEvent *) {
		simulateTick();
	}

	void QtUI::addFileToList(const QString &Filename, QListWidgetItem *before) {
		int id = before ? file_list->row(before) + 1 : file_list->count();
		file_list->insertItem(id, Filename);
		QListWidgetItem *item = file_list->item(id);
		item->setSizeHint(QSize(0, 42));

		QWidget *widget = new QWidget;
		QHBoxLayout *layout = new QHBoxLayout(widget);
		layout->addStretch();
		for (QString name : {"list-add.svg", "list-remove.svg"}) {
			QPushButton *button = new QPushButton(getIcon(name), "");
			button->setFlat(true);
			if (name.contains("add"))
				connect(button, &QPushButton::clicked, [this, item](bool) {
					addFileToList(item->text(), item);
				});
			else
				connect(button, &QPushButton::clicked, [this, item](bool) {
					file_list->removeItemWidget(item);
					delete item;
				});
			layout->addWidget(button);
		}
		file_list->setItemWidget(item, widget);
	}

	void QtUI::loadBaseHill(int index) {
		hill_variables->setRowCount(0);
		for (const auto &Pair : *corewar::Hills[index])
			addVariableToHill(Pair.first.c_str(), QString::number(Pair.second));
		addLastRowToHill();
	}

	void QtUI::addVariableToHill(const QString &Name, const QString &Value) {
		int row = hill_variables->rowCount();
		hill_variables->setRowCount(row + 1);
		QTableWidgetItem *ptr;
		hill_variables->setItem(row, 0, ptr = new QTableWidgetItem(Name));
		hill_variables->setItem(row, 1, new QTableWidgetItem(Value));
		{
			QPushButton *button = new QPushButton(getIcon("list-remove.svg"), "");
			button->setFlat(true);
			connect(button, &QPushButton::clicked, [this, ptr](bool) {
				QList<QPair<QString, QString>> list;
				int row = hill_variables->row(ptr);
				for (int i = row + 1; i < hill_variables->rowCount() - 1; ++i)
					list.append(qMakePair(
							hill_variables->item(i, 0)->text(),
							hill_variables->item(i, 1)->text()));
				hill_variables->setRowCount(row);
				for (const auto Pair : list)
					addVariableToHill(Pair.first, Pair.second);
				addLastRowToHill();
			});
			hill_variables->setCellWidget(row, 2, button);
		}
	}

	void QtUI::addLastRowToHill() {
		int row = hill_variables->rowCount();
		hill_variables->setRowCount(row + 1);
		QLineEdit *name = new QLineEdit, *value = new QLineEdit;
		name->setValidator(name_validator);
		value->setValidator(number_validator);
		QPushButton *button = new QPushButton(getIcon("list-add.svg"), "");
		button->setFlat(true);
		connect(button, &QPushButton::clicked, [this, name, value](bool) {
			if (!name->hasAcceptableInput() || !value->hasAcceptableInput()
					|| hill_variables->findItems(name->text(), Qt::MatchExactly).size())
				return;
			hill_variables->setRowCount(hill_variables->rowCount() - 1);
			addVariableToHill(name->text(), value->text());
			addLastRowToHill();
		});
		hill_variables->setCellWidget(row, 0, name);
		hill_variables->setCellWidget(row, 1, value);
		hill_variables->setCellWidget(row, 2, button);
	}

	void QtUI::setUpArenaInterface() {
		// create hill for compilation
		auto hill = std::make_shared<corewar::Hill>();
		for (int row = 0; row < hill_variables->rowCount() - 1; ++row) {
			std::string key = hill_variables->item(row, 0)->text().toStdString();
			if (hill->count(key)) {
				error->showMessage(QStringLiteral("Environment variable \"")
						% key.c_str() % "\" was already defined. Using old value.");
				continue;
			}
			bool ok;
			int value = hill_variables->item(row, 1)->text().toInt(&ok, 0);
			if (!ok) {
				error->showMessage(QStringLiteral("Cannot convert value of \"")
						% key.c_str() % "\" to int. Set it to 0.");
				continue;
			}
			(*hill)[key] = value;
		}

		// assemble supplied warriors
		QStringList failed;
		QMap<QString, corewar::Warrior> map;
		for (int i = 0; i < file_list->count(); ++i) {
			QString file = file_list->item(i)->text();
			if (map.contains(file))
				continue;
			std::shared_ptr<corewar::AssemblerInterface> assembler
					= std::make_shared<assembler::RedcodeAssembler>(hill);
			try {
				assembler->parseFile(file.toStdString());
			} catch (const corewar::AssertionError &E) {
				error->showMessage(file % ": " % E.what());
			} catch (const corewar::AssemblerError &E) {
				failed.append(file % ": " % E.what());
				continue;
			}
			map.insert(file, assembler->getWarrior());
		}
		if (!failed.isEmpty()) {
			QMessageBox::critical(this, "Assembler", "Cannot compile:\n" + failed.join("\n"));
			return;
		}

		// create ArenaInterface
		try {
			arena = std::make_shared<arena::CorewarArena>(hill);
			if (hill->at("WARRIORS") != file_list->count()
					|| unsigned(hill->at("WARRIORS") - 1) > 9)
				throw corewar::Exception("invalid number of warriors");
			for (int i = 0; i < file_list->count(); ++i)
				arena->addWarrior(map.value(file_list->item(i)->text()));
		} catch (const corewar::Exception &E) {
			QMessageBox::critical(
					this, "Arena", QStringLiteral("Cannot set up arena:\n") + E.what());
			return;
		}

		// initialize necessary variables
		cc = corewar::CoordinatesConverter(arena->size());
		warriors_names.clear();
		for (int i = 0; i < file_list->count(); ++i) {
			QString filename = file_list->item(i)->text();
			try {
				warriors_names.append(map.value(filename)
						.comments.at(CommentType::NAME).c_str());
			} catch (const std::out_of_range &) {
				warriors_names.append(filename.section('/', -1));
			}
		}

		// set up graphical interface

		// set up warriors_info
		for (QFormLayout *layout : findLayoutChildren<QFormLayout>(warriors_info))
			for (QLabel *label : findLayoutChildren<QLabel>(layout))
				label->hide();
		for (QLayoutItem *child; (child = warriors_info->takeAt(0)) != nullptr;)
			delete child;
		for (int i = 0; i < warriors_names.size(); ++i) {
			QFormLayout *layout = new QFormLayout;
			QLabel *label = new QLabel;
			label->setPixmap(tiles[static_cast<int>(TileOffset::SINGLE) + i]);
			layout->addRow(label, new QLabel(warriors_names[i]));
			layout->addRow("count:", new QLabel("1"));
			layout->addRow("next:", new QLabel(QString::number(arena->queueFront(i + 1))));
			layout->addRow("wins:", new QLabel("0"));
			warriors_info->addLayout(layout);
		}

		// set up arena_view
		arena_items.clear();
		delete std::exchange(arena_scene,
				new QGraphicsScene(0, 0, cc.width * 8, cc.height * 8, this));
		arena_view->setScene(arena_scene);
		selected = QPoint(1, 1);
		for (int y = 0; y < cc.height; ++y)
			for (int x = 0; x < cc.width; ++x) {
				arena_items.append(arena_scene->addPixmap(tile(cc(x, y))));
				arena_items.last()->setOffset(x * 8, y * 8);
			}

		// set up instruction_edit
		bool bound = qobject_cast<QCheckBox *>(instruction_edit->itemAt(3, QFormLayout::SpanningRole)->widget())
			->checkState() == Qt::Checked;
		for (int i = 0; i < 2; ++i)
			boundSpinBox(
					qobject_cast<QSpinBox *>(instruction_edit->itemAt(i + 1, QFormLayout::FieldRole)->widget()),
					arena->size(),
					bound);

		// set up pspace_edit
		{
			bool enable = arena->pspaceSize() && arena->living();
			auto on_off = [=](auto &&list) {
				for (const auto &Widget : list)
					Widget->setVisible(enable);
			};
			pspace_label->setText("P-space (max: "
					% QString::number(arena->pspaceSize() - 1) % ")");
			pspace_label->setVisible(enable);
			on_off(findLayoutChildren<QComboBox>(pspace_edit));
			on_off(findLayoutChildren<QLabel>(pspace_edit));
			on_off(findLayoutChildren<QSpinBox>(pspace_edit));
			on_off(findLayoutChildren<QCheckBox>(pspace_edit));
			if (enable) {
				{
					QComboBox *warriors = qobject_cast<QComboBox *>(
							pspace_edit->itemAt(0, QFormLayout::SpanningRole)->widget());
					while (warriors->count())
						warriors->removeItem(0);
					warriors->addItems(warriors_names.toList());
				}
				selected_pspace = qMakePair(1, 0);
				{
					auto spinboxes = findLayoutChildren<QSpinBox>(pspace_edit);
					QList<bool> bounds;
					for (const auto &CheckBox : findLayoutChildren<QCheckBox>(pspace_edit))
						bounds.append(CheckBox->checkState() == Qt::Checked);
					spinboxes.at(0)->setValue(0);
					boundSpinBox(
							spinboxes.at(0),
							bounds.at(0) ? arena->pspaceSize() : arena->size(),
							bounds.at(0));
					boundSpinBox(spinboxes.at(1), arena->size(), bounds.at(1));
				}
				updatedPspaceCell();
			}
		}

		// finalize
		selectArenaCell(QPoint());
		step_label->setText("step: 0");
		next_round->setEnabled(false);
		main_widget->setCurrentIndex(1);
	}

	void QtUI::selectArenaCell(QPoint P) {
		P = QPoint(qBound(0, P.x(), cc.width - 1), qBound(0, P.y(), cc.height - 1));
		int from = cc(selected.x(), selected.y()), to = cc(P.x(), P.y());
		if (from == to)
			return;
		selected = P;
		updatedArenaOffset(from);
		updatedArenaOffset(to);
		arena_label->setText("core (offset: " % QString::number(to) % ")");
	}

	void QtUI::updatedArenaOffset(int offset) {
		// update arena_view
		arena_items[offset]->setPixmap(tile(offset));
		if (offset != cc(selected.x(), selected.y()))
			return;

		// update instruction_edit
		Instruction instruction = arena->getCoreAt(offset);
		{
			auto set_checkboxes = [&](int row, QFormLayout::ItemRole role, auto &&ptr) {
				qobject_cast<QComboBox *>(instruction_edit->itemAt(row, role)->widget())
					->setCurrentIndex(static_cast<int>(instruction.opcode.*ptr));
			};
			set_checkboxes(0, QFormLayout::LabelRole, &Opcode::operation);
			set_checkboxes(0, QFormLayout::FieldRole, &Opcode::modifier);
			set_checkboxes(1, QFormLayout::LabelRole, &Opcode::addrModeA);
			set_checkboxes(2, QFormLayout::LabelRole, &Opcode::addrModeB);
		}
		{
			bool bound = qobject_cast<QCheckBox *>(instruction_edit->itemAt(3, QFormLayout::SpanningRole)->widget())
				->checkState() == Qt::Checked;
			for (int i = 0; i < 2; ++i) {
				int value = instruction.*(i ? &Instruction::fieldB : &Instruction::fieldA);
				qobject_cast<QSpinBox *>(instruction_edit->itemAt(i + 1, QFormLayout::FieldRole)->widget())
					->setValue(bound
							? value
							: (value > arena->size() / 2 ? value - arena->size() : value));
			}
		}

		// update processes_info
		for (QLabel *label : findLayoutChildren<QLabel>(processes_info))
			label->hide();
		for (QLayoutItem *child; (child = processes_info->takeAt(0)) != nullptr;)
			delete child;
		{
			auto vec = arena->executedBy(offset);
			for (int i = 1; i <= int(vec.size()); ++i) {
				const auto &P = vec[vec.size() - i];
				QLabel *label = new QLabel;
				label->setPixmap(tiles[static_cast<int>(TileOffset::SINGLE) + P.first - 1]);
				processes_info->addWidget(label, i, 0);
				processes_info->addWidget(new QLabel(warriors_names[P.first - 1]), i, 1);
				processes_info->addWidget(new QLabel(QString::number(P.second)), i, 2);
			}
		}
	}

	void QtUI::updatedPspaceCell() {
		bool bound = qobject_cast<QCheckBox *>(pspace_edit->itemAt(4, QFormLayout::SpanningRole)->widget())
			->checkState() == Qt::Checked;
		int value = arena->getPspaceAt(selected_pspace.first, selected_pspace.second);
		qobject_cast<QSpinBox *>(pspace_edit->itemAt(2, QFormLayout::FieldRole)->widget())
			->setValue(bound
					? value
					: (value > arena->size() / 2 ? value - arena->size() : value));
	}

	void QtUI::unlockWidgets(bool state) {
		auto on_off = [=](auto &&list) {
			for (const auto &Widget : list)
				Widget->setEnabled(state);
		};
		on_off(findLayoutChildren<QComboBox>(instruction_edit));
		on_off(findLayoutChildren<QSpinBox>(instruction_edit));
		pspace_edit->itemAt(2, QFormLayout::FieldRole)->widget()->setEnabled(state);
	}

	void QtUI::simulateTick() {
		if (arena->living() < 2 || arena->cycle() >= arena->maxCycles()) {
			killTimer(std::exchange(timer, 0));
			unlockWidgets(debug);
			next_round->setEnabled(true);
			return;
		}

		for (int offset : arena->tick())
			updatedArenaOffset(offset);
		step_label->setText("step: " + QString::number(arena->cycle()));
		if (arena->pspaceSize())
			updatedPspaceCell();

		int id = 1;
		for (const auto &Layout : findLayoutChildren<QFormLayout>(warriors_info)) {
			auto get_label = [=](int row) {
				return qobject_cast<QLabel *>(
						Layout->itemAt(row, QFormLayout::FieldRole)->widget());
			};
			if (get_label(1)->text() != "0") {
				get_label(1)->setText(QString::number(arena->queueSize(id)));
				get_label(2)->setText(arena->queueSize(id)
						? QString::number(arena->queueFront(id))
						: "-");
			}
			++id;
		}

		if (arena->living() < 2 || arena->cycle() >= arena->maxCycles())
			simulateTick();
	}

	void QtUI::shiftInterval(bool up) {
		if (timer && ((up && interval < 0x40000000) || (!up && interval > 1))) {
			killTimer(timer);
			timer = startTimer(up ? interval <<= 1 : interval >>= 1);
		}
	}

	QPixmap QtUI::tile(int offset) {
		auto vec = arena->executedBy(offset);
		int id = (offset == cc(selected.x(), selected.y())) << 10
				| static_cast<int>(arena->tile(offset)) << 8
				| arena->lastModifiedBy(offset) << 4
				| (vec.size() ? vec[0].first | (vec.size() > 1) * 0xf : 0);
		if (cached_pixmaps.contains(id))
			return cached_pixmaps.value(id);

		QPixmap pixmap(8, 8);
		QPainter painter(&pixmap);
		painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
		auto draw = [&](int offset) { painter.drawPixmap(0, 0, tiles[offset]); };
		draw(static_cast<int>(TileOffset::TILES) + static_cast<int>(arena->tile(offset)));
		if (arena->lastModifiedBy(offset))
			draw(static_cast<int>(TileOffset::MODIFIED) + arena->lastModifiedBy(offset) - 1);
		if (vec.size() > 1)
			draw(static_cast<int>(TileOffset::MULTIPLE));
		else if (vec.size())
			draw(static_cast<int>(TileOffset::SINGLE) + vec[0].first - 1);
		if (offset == cc(selected.x(), selected.y()))
			draw(static_cast<int>(TileOffset::SELECTED));
		cached_pixmaps.insert(id, pixmap);
		return cached_pixmaps.value(id);
	}
}
