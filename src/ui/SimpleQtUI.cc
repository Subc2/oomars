#include <QApplication>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>

#include "SimpleQtUI.hh"

namespace ui {
	SimpleQtUI::SimpleQtUI(std::shared_ptr<corewar::ArenaInterface> arena, QPixmap *tileset)
			: arena(arena), cc(arena->size()), tileset(tileset),
			  timer(0), gameover(false), delay(0x100) {
		setSceneRect(0, 0, cc.width * 8, cc.height * 8);
		for (int i = 0; i < 9; ++i)
			tiles[i] = tileset->copy(i * 8, 0, 8, 8);
		graphics_items = new QGraphicsPixmapItem **[cc.height];
		for (int y = 0; y < cc.height; ++y)
			graphics_items[y] = new QGraphicsPixmapItem *[cc.width];
		for (int y = 0; y < cc.height; ++y)
			for (int x = 0; x < cc.width; ++x) {
				graphics_items[y][x] = addPixmap(tile(cc(x, y)));
				graphics_items[y][x]->setOffset(x * 8, y * 8);
			}
		timer = startTimer(delay);
	}

	SimpleQtUI::~SimpleQtUI() {
		for (int y = 0; y < cc.height; ++y)
			delete [] graphics_items[y];
		delete [] graphics_items;
	}

	void SimpleQtUI::keyPressEvent(QKeyEvent *event) {
		switch (event->key()) {
			case Qt::Key_Q:
				qApp->quit();
				break;
			case Qt::Key_M:
				if (timer == 0)
					timer = startTimer(delay);
				else
					killTimer(std::exchange(timer, 0));
				break;
			case Qt::Key_Space:
				if (!gameover)
					simulateTick();
				break;
			case Qt::Key_Plus:
				if (delay > 1 && timer != 0) {
					killTimer(timer);
					timer = startTimer(delay >>= 1);
				}
				break;
			case Qt::Key_Minus:
				if (delay < 0x40000000 && timer != 0) {
					killTimer(timer);
					timer = startTimer(delay <<= 1);
				}
				break;
		}
	}

	void SimpleQtUI::timerEvent(QTimerEvent *) {
		if (!gameover)
			simulateTick();
	}

	void SimpleQtUI::simulateTick() {
		if (arena->living() > 1)
			for (int offset : arena->tick()) {
				auto coords = cc(offset);
				graphics_items[coords.y][coords.x]->setPixmap(tile(coords));
			}
		else {
			gameOver();
			gameover = true;
			killTimer(std::exchange(timer, 0));
		}
	}

	void SimpleQtUI::gameOver() {
		QString msg;
		try {
			msg = QString("Warrior \"%1\" has won.").arg(
					arena->getWarrior(arena->movesNext()).comments
					.at(corewar::CommentType::NAME).c_str());
		} catch (const std::out_of_range &) {
			msg = "Tie.";
		}
		QGraphicsTextItem *text = addText(msg, QFont("DejaVu", 30));
		QRect where = text->boundingRect().toAlignedRect();
		where.translate((width() - where.width()) / 2, (height() - where.height()) / 2);
		QGraphicsRectItem *bg = addRect(where, QPen(Qt::red), QBrush(Qt::red));
		text->setPos(where.topLeft());
		bg->stackBefore(text);
	}

	const QPixmap &SimpleQtUI::tile(int offset) const {
		auto vec = arena->executedBy(offset);
		if (vec.size() > 1)
			return tiles[3];
		if (vec.size() == 1)
			return tiles[vec[0].first];
		auto last = arena->lastModifiedBy(offset);
		if (!last)
			return tiles[0];
		switch (arena->tile(offset)) {
			case corewar::Tile::DATA:
				return tiles[5 + last];
			case corewar::Tile::SPLIT:
				return tiles[8];
			default:
				break;
		}
		return tiles[3 + last];
	}
}
