#include <cstdio>
#include <unistd.h>

#include "TextUI.hh"

namespace ui {
	TextUI::TextUI(std::shared_ptr<corewar::ArenaInterface> arena, int delay)
			: arena(arena),
			  cc(arena->size()),
			  delay(delay),
			  arena_repr(cc.height * (cc.width + 1) + 1) {}

	void TextUI::exec() {
		fill();
		puts(arena_repr.data());
		while (arena->living() > 1) {
			usleep(delay * 1000);
			update(arena->tick());
			puts(arena_repr.data());
		}
		try {
			printf("Warrior \"%s\" has won.\n",
					arena->getWarrior(arena->movesNext()).comments
					.at(corewar::CommentType::NAME).c_str());
		} catch (const std::out_of_range &) {
			puts("Tie.");
		}
	}

	char TextUI::charRepr(int offset) const {
		auto vec = arena->executedBy(offset);
		if (vec.size() > 1)
			return '+';
		if (vec.size() == 1)
			return 'A' + vec[0].first - 1;
		auto last = arena->lastModifiedBy(offset);
		if (!last)
			return '.';
		switch (arena->tile(offset)) {
			case corewar::Tile::DATA:
				return '0' + last % 10;
			case corewar::Tile::SPLIT:
				return '!';
			default:
				return 'a' + last - 1;
		}
	}

	void TextUI::fill() {
		for (int y = 0; y < cc.height; ++y) {
			for (int x = 0; x < cc.width; ++x)
				arena_repr[y * (cc.width + 1) + x] = charRepr(cc(x, y));
			arena_repr[y  * (cc.width + 1) + cc.width] = '\n';
		}
		arena_repr[cc.height * (cc.width + 1) - 1] = '\n';
		arena_repr[cc.height * (cc.width + 1)] = '\0';
	}

	void TextUI::update(const std::vector<int> &Changed) {
		for (int offset : Changed) {
			auto coords = cc(offset);
			arena_repr[coords.y * (cc.width + 1) + coords.x] = charRepr(coords);
		}
	}
}
