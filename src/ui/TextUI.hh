#pragma once

#include "../corewar.hh"

namespace ui {
	class TextUI {
		private:
			std::shared_ptr<corewar::ArenaInterface> arena;
			corewar::CoordinatesConverter cc;
			int delay;
			std::vector<char> arena_repr;

		public:
			TextUI(std::shared_ptr<corewar::ArenaInterface>, int);
			void exec();

		private:
			char charRepr(int) const;
			void fill();
			void update(const std::vector<int> &);
	};
}
