#pragma once

#include <array>
#include <cstdint>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace corewar {
	enum class Operation : int8_t { DAT, MOV, ADD, SUB, MUL, DIV, MOD, JMP, JMZ, JMN, DJN, SPL, CMP, SEQ, SNE, SLT, LDP, STP, NOP };
	enum class Modifier  : int8_t { A, B, AB, BA, F, X, I };
	enum class AddrMode  : int8_t { IMM, DIR, A_IND, B_IND, A_IND_PRE, B_IND_PRE, A_IND_POS, B_IND_POS };

	struct Opcode {
		Operation operation;
		Modifier  modifier;
		AddrMode  addrModeA;
		AddrMode  addrModeB;

		Opcode();
		Opcode(const std::string &, const std::string &);
		Opcode(const std::string &, const std::string &, const std::string &);
		Opcode(Operation, Modifier, AddrMode, AddrMode);
		bool operator!=(const Opcode &) const;
		bool operator==(const Opcode &) const;
	};

	struct Instruction {
		Opcode opcode;
		int    fieldA;
		int    fieldB;

		Instruction();
		Instruction(Opcode, int);
		Instruction(Opcode, int, int);
		bool operator!=(const Instruction &) const;
		bool operator==(const Instruction &) const;
	};

	enum class CommentType { REDCODE, NAME, AUTHOR, VERSION, DATE, STRATEGY, KILL };

	struct Warrior : public std::vector<Instruction> {
		int origin;
		int pin;
		std::map<CommentType, std::string> comments;

		Warrior();
	};

	typedef std::map<std::string, int> Hill;
	enum class HillType { R94, R94NOP, R94X, R94M, R94XM };

	extern const int Version;
	extern const std::array<std::shared_ptr<const Hill>, 5> Hills;

	struct CoordinatesConverter {
		struct Coordinates {
			int offset, x, y;
			operator int() const;
		};

		int size, width, height;

		explicit CoordinatesConverter(int);
		CoordinatesConverter(int, int);
		Coordinates operator()(int) const;
		Coordinates operator()(int, int) const;
	};

	enum class Tile { CODE, DATA, JUMP, SPLIT };

	struct ArenaInterface {
		virtual ~ArenaInterface() = default;
		virtual void addWarrior(const Warrior &) = 0;
		virtual const Warrior &getWarrior(uint8_t) const = 0;
		virtual int queueSize(uint8_t) const = 0;
		virtual int queueFront(uint8_t) const = 0;

		virtual int size() const = 0;
		virtual int pspaceSize() const = 0;
		virtual int cycle() const = 0;
		virtual int maxCycles() const = 0;
		virtual Tile tile(int) const = 0;
		virtual int living() const = 0;
		virtual uint8_t movesNext() const = 0;

		virtual const Instruction &getCoreAt(int) const = 0;
		virtual void setCoreAt(int, Instruction) = 0;
		virtual int getPspaceAt(uint8_t, int) const = 0;
		virtual void setPspaceAt(uint8_t, int, int) = 0;

		virtual uint8_t lastModifiedBy(int) const = 0;
		virtual std::vector<std::pair<uint8_t, int>> executedBy(int) const = 0;
		virtual std::vector<int> tick() = 0;
		virtual void nextRound() = 0;
	};

	struct AssemblerInterface {
		virtual ~AssemblerInterface() = default;
		virtual void parseFile(const std::string &) = 0;
		virtual Warrior getWarrior() const = 0;
	};

	struct Exception : public std::runtime_error {
		explicit Exception(const std::string &);
	};

	struct ArenaError : public Exception {
		explicit ArenaError(const std::string &);
	};

	struct AssemblerError : public Exception {
		explicit AssemblerError(const std::string &);
	};

	struct AssertionError : public Exception {
		explicit AssertionError(const std::string &);
	};
}
