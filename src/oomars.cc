#include <QApplication>
#include <QGraphicsView>

#include <cerrno>
#include <cstdio>
#include <getopt.h>
#include <list>

#include "arena.hh"
#include "assembler.hh"
#include "ui.hh"

std::string to_version_string(int n) {
	std::string s = std::to_string(n) + "..";
	std::swap(s[s.size() - 4], s[s.size() - 1]);
	std::swap(s[s.size() - 3], s[s.size() - 1]);
	return s;
}

int main(int argc, char *argv[]) {
	const struct option LongOptions[] = {
		{"assemble", 1, NULL, 'a'},
		{"delay",    1, NULL, 'd'},
		{"help",     0, NULL, 'h'},
		{"simple",   0, NULL, 's'},
		{"text",     0, NULL, 't'},
		{"usage",    0, NULL, 'u'},
		{"version",  0, NULL, 'v'},
		{NULL, 0, NULL, 0}
	};

	int delay = 0x100;
	bool run_simple = false, run_text = false;
	std::shared_ptr<corewar::Hill> hill = std::make_shared<corewar::Hill>(
			*corewar::Hills[static_cast<int>(corewar::HillType::R94M)]);
	std::list<std::string> warriors_files;
	std::list<corewar::Warrior> warriors;

	for (int option, long_option_index; (option = getopt_long(argc, argv, "a:d:hstuv", LongOptions, &long_option_index)) != -1;)
		switch (option) {
			case 'a':
				try {
					std::shared_ptr<corewar::AssemblerInterface> assembler
							= std::make_shared<assembler::RedcodeAssembler>(corewar::HillType::R94);
					assembler->parseFile(optarg);
				} catch (const corewar::AssertionError &) {
				} catch (const std::exception &E) {
					fprintf(stderr, "%s: %s\n", optarg, E.what());
					return EPERM;
				}
				return 0;
			case 'd':
				if (sscanf(optarg, "%d", &delay) != 1 || delay < 0) {
					fprintf(stderr, "Invalid delay parameter \"%s\".\n", optarg);
					return EINVAL;
				}
				break;
			case 'h':
				puts("OoMARS - Core War game implementation\n"
				"Syntax: oomars [arguments] [warrior ...]\n"
				"Arguments:\n"
				"  -s, --simple              show simple interface\n"
				"  -t, --text                start in text mode\n"
				"  -d TIME, --delay TIME     set delay time for the battle in miliseconds\n"
				"  -a FILE, --assemble FILE  only assemble file on the ICWS\'94 hill; ignore \";assert\"s\n"
				"  -h, --help                print this help text\n"
				"  -u, --usage               short usage information\n"
				"  -v, --version             display program version");
				return 0;
			case 's':
				run_simple = true;
				break;
			case 't':
				run_text = true;
				break;
			case 'u':
				puts("Syntax: oomars [-s|--simple] [-t|--text] [-d TIME|--delay TIME] [WARRIOR ...]\n"
				"               [-a FILE|--assemble FILE]\n"
				"               [-h|--help] [-u|--usage] [-v|--version]");
				return 0;
			case 'v':
				printf("OoMARS version %s\nCopyright (C) 2017-2021 Paweł Zacharek <subc2@wp.pl>\n",
						to_version_string(corewar::Version).c_str());
				return 0;
			default:
				return EINVAL;
		}
	if (optind <= argc - 1 && argc - 10 <= optind) {  // 1-10 unknown parameters (warriors file names)
		(*hill)["WARRIORS"] = argc - optind;
		while (optind < argc)
			warriors_files.emplace_back(argv[optind++]);
	} else if (optind != argc) {  // more than 10 unknown parameters
		fputs("Invalid argument(s) found.\n", stderr);
		return EINVAL;
	}

	if ((!run_simple && !run_text) || warriors_files.size() < 2) {
		QApplication app(argc, argv);
		ui::QtUI window(warriors_files);
		window.show();
		return app.exec();
	}

	std::shared_ptr<corewar::AssemblerInterface> assembler
			= std::make_shared<assembler::RedcodeAssembler>(hill);
	for (const auto &Filename : warriors_files) {
		try {
			assembler->parseFile(Filename);
		} catch (const corewar::AssemblerError &E) {
			fprintf(stderr, "Error during assembling \"%s\":\n%s\n",
					Filename.c_str(), E.what());
			return EPERM;
		}
		warriors.push_back(assembler->getWarrior());
		if (!warriors.back().comments.count(corewar::CommentType::NAME))
			warriors.back().comments[corewar::CommentType::NAME] = Filename;
	}

	std::shared_ptr<corewar::ArenaInterface> arena
			= std::make_shared<arena::CorewarArena>(hill);
	try {
		for (const auto &Warrior : warriors)
			arena->addWarrior(Warrior);
	} catch (const corewar::ArenaError &E) {
		fprintf(stderr, "Error during arena setting:\n%s\n", E.what());
		return EPERM;
	}

	if (!run_text && warriors_files.size() < 3) {
		QApplication app(argc, argv);
		QPixmap tileset(":/tileset_simple.png");
		if (tileset.isNull()) {
			fputs("Cannot open tileset \"tileset_simple.png\".\n", stderr);
			return ENOENT;
		}
		ui::SimpleQtUI scene(arena, &tileset);
		QGraphicsView view(&scene);
		view.show();
		return app.exec();
	}

	ui::TextUI ui(arena, delay);
	ui.exec();
	return 0;
}
