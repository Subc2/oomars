#include <algorithm>
#include <fstream>
#include <regex>

#include "SimpleRedcodeAssembler.hh"

using corewar::Warrior;
using corewar::AssemblerError;

namespace {
	inline int mod(int x, int y) {
		x %= y;
		return x + (x < 0) * y;
	}

	inline bool setInt(int &value, const std::string &str) {
		try {
			value = std::stoi(str, 0, std::isalpha(str[1]) ? 16 : 10);
		} catch (...) {
			return false;
		}
		return true;
	}

	Warrior createWarrior(std::istream &in, int coresize) {
		Warrior warrior;
		std::map<std::string, int> labels;
		std::smatch matches;
		std::string line;

		if (!coresize)
			throw AssemblerError("CORESIZE cannot be 0");

		for (int lineNumber = 1, offset = 0; std::getline(in, line); ++lineNumber) {
			if (std::regex_search(line, matches, std::regex(
					"^\\s*([a-zA-Z_]+):"   // label [1]
			))) {
				if (labels.count(matches[1].str()))
					throw AssemblerError(
							std::to_string(lineNumber)
							+ ": redefinition of label \"" + matches[1].str() + "\"");
				labels[matches[1].str()] = offset;
			}
			if (std::regex_search(line, std::regex(
					"^\\s*([a-zA-Z_]+:)?"  // label (optional)
					"\\s*[a-zA-Z]{3}"      // operation
					"(\\.[a-zA-Z]{1,2})?"  // modifier (optional)
					"\\s+[#$*<>@{}]?"      // addressing mode (optional)
					"[-0-9a-zA-Z_]+"       // A-value
			)))
				++offset;
		}

		in.clear();
		in.seekg(0);

		for (int lineNumber = 1; std::getline(in, line); ++lineNumber)
			if (std::regex_match(line, matches, std::regex(
					"\\s*([a-zA-Z_]+:)?"  // label (optional)
					"\\s*("
					  "[a-zA-Z]{3}"       // operation                  [2]
					  "(\\.("             // and modifier (optional)
					    "[FXIfxi]"
					    "|[Aa][Bb]?"
					    "|[Bb][Aa]?"
					  "))?"
					")"
					"\\s+([#$*<>@{}])?"   // addressing mode (optional) [5]
					"([-0-9a-zA-Z_]+)"    // A-value                    [6]
					"("
					  "\\s*,\\s*"         // comma and optional spaces
					  "([#$*<>@{}])?"     // addressing mode (optional) [8]
					  "([-0-9a-zA-Z_]+)"  // B-value                    [9]
					")?"
					"\\s*(;.*)?"          // whitespace characters and comment (optional)
			))) {
				std::string operation = matches[2].str();
				std::transform(operation.begin(), operation.end(), operation.begin(), toupper);

				corewar::Instruction instruction;
				try {
					instruction.opcode =
							corewar::Opcode(operation, matches[5].str(), matches[8].str());
				} catch (const corewar::Exception &E) {
					throw AssemblerError(std::to_string(lineNumber) + ": " + E.what());
				}

				const auto &label_a = labels.find(matches[6].str().c_str());
				const auto &label_b = labels.find(matches[9].str().c_str());

				if (label_a != labels.end())
					instruction.fieldA = -warrior.size() + label_a->second;
				else if (!setInt(instruction.fieldA, matches[6].str()))
					throw AssemblerError(
							std::to_string(lineNumber)
							+ ": unknown A-value \"" + matches[6].str() + "\"");

				if (label_b != labels.end())
					instruction.fieldB = -warrior.size() + label_b->second;
				else if (matches[9].str() == "")
					instruction.fieldB = 0;
				else if (!setInt(instruction.fieldB, matches[9].str()))
					throw AssemblerError(
							std::to_string(lineNumber)
							+ ": unknown B-value \"" + matches[9].str() + "\"");

				if (instruction.opcode.operation == corewar::Operation::DAT
						&& matches[6].str() != "" && matches[9].str() == "") {
					std::swap(instruction.opcode.addrModeA, instruction.opcode.addrModeB);
					std::swap(instruction.fieldA, instruction.fieldB);
				}

				instruction.fieldA = mod(instruction.fieldA, coresize);
				instruction.fieldB = mod(instruction.fieldB, coresize);
				warrior.push_back(instruction);
			} else if (!std::regex_match(line, std::regex(
					"\\s*([a-zA-Z_]+:)?"  // label (optional)
					"\\s*(;.*)?"          // whitespace characters and comment (optional)
			)))
				throw AssemblerError(std::to_string(lineNumber) + ": syntax error");

		return warrior;
	}
}

namespace assembler {
	SimpleRedcodeAssembler::SimpleRedcodeAssembler(corewar::HillType type)
			: SimpleRedcodeAssembler(corewar::Hills[static_cast<int>(type)]) {}

	SimpleRedcodeAssembler::SimpleRedcodeAssembler(const std::shared_ptr<const corewar::Hill> &Hill)
			: hill(Hill) {}

	void SimpleRedcodeAssembler::parseFile(const std::string &fileName) {
		std::ifstream in(fileName);
		if (!in.is_open())
			throw AssemblerError("cannot open file");
		warrior = createWarrior(in, hill->at("CORESIZE"));
	}

	Warrior SimpleRedcodeAssembler::getWarrior() const {
		return warrior;
	}
}
