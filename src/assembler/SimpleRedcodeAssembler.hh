#pragma once

#include "../corewar.hh"

namespace assembler {
	class SimpleRedcodeAssembler : public corewar::AssemblerInterface {
		private:
			std::shared_ptr<const corewar::Hill> hill;
			corewar::Warrior warrior;

		public:
			SimpleRedcodeAssembler(corewar::HillType = corewar::HillType::R94);
			SimpleRedcodeAssembler(const std::shared_ptr<const corewar::Hill> &);
			void parseFile(const std::string &) override;
			corewar::Warrior getWarrior() const override;
	};
}
