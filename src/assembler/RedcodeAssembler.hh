#pragma once

#include "../corewar.hh"

namespace assembler {
	class RedcodeAssembler : public corewar::AssemblerInterface {
		private:
			std::shared_ptr<const corewar::Hill> hill;
			corewar::Warrior warrior;

		public:
			RedcodeAssembler(corewar::HillType = corewar::HillType::R94);
			RedcodeAssembler(const std::shared_ptr<const corewar::Hill> &);
			void parseFile(const std::string &) override;
			corewar::Warrior getWarrior() const override;
	};
}
