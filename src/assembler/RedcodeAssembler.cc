#include <fstream>

#include "RedcodeAssembler.hh"
#include "../parser/RedcodeParser.hh"

using corewar::AssemblerError;

namespace assembler {
	RedcodeAssembler::RedcodeAssembler(corewar::HillType type)
			: RedcodeAssembler(corewar::Hills[static_cast<int>(type)]) {}

	RedcodeAssembler::RedcodeAssembler(const std::shared_ptr<const corewar::Hill> &Hill)
			: hill(Hill) {}

	void RedcodeAssembler::parseFile(const std::string &fileName) {
		std::ifstream in(fileName);
		if (!in.is_open())
			throw AssemblerError("cannot open file");
		parser::RedcodeParser parser(hill, in);
		try {
			if (parser.parse())
				throw AssemblerError("syntax error");
			parser.assemble();
		} catch (corewar::AssertionError &) {
			warrior = parser.warrior;
			throw;
		}
		warrior = parser.warrior;
	}

	corewar::Warrior RedcodeAssembler::getWarrior() const {
		return warrior;
	}
}
