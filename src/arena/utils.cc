#include <cassert>

#include <gmp.h>
#include <gmpxx.h>

#include "utils.hh"

namespace {
	mpz_class binomial(int n, int k) {
		mpz_class rop;
		mpz_bin_uiui(rop.get_mpz_t(), n, k);
		return rop;
	}
}

namespace arena {
	void fillWithNumbersSummingTo(std::vector<int> &vec, int sum, std::mt19937_64 &rne) {
		gmp_randclass rand(gmp_randinit_default);
		rand.seed(rne());
		mpz_class
				possibilities = binomial(sum + vec.size() - 1, vec.size() - 1),
				index = rand.get_z_range(possibilities);  // including the 0-th possibility

		for (int n = vec.size(); n > 0; --n) {
			int l, m, r;
			/* binary search; exactly the same result can be accomplished by:
			 * for (l = sum; binomial((sum - l) + (n - 1), n - 1) <= index; --l)
			 *   continue;
			 */
			for (l = 0, r = sum, m = (r + 1) >> 1; l < r; m = (l + r + 1) >> 1)
				// https://en.wikipedia.org/wiki/Hockey-stick_identity
				binomial((sum - m) + (n - 1), n - 1) <= index ? r = m - (r == m) : l = m;
			index -= binomial((sum - l - 1) + (n - 1), n - 1);
			sum -= vec[n - 1] = l;
		}
		assert(!sum);
	}

	void throwOnMisssingKey(const corewar::Hill &Hill, std::initializer_list<std::string> &&list) {
		for (const auto &key : list)
			if (!Hill.count(key))
				throw corewar::ArenaError("missing \"" + key + "\" hill variable");
	}
}
