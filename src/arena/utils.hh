#pragma once

#include <random>

#include "../corewar.hh"

namespace arena {
	void fillWithNumbersSummingTo(std::vector<int> &, int, std::mt19937_64 &);
	void throwOnMisssingKey(const corewar::Hill &, std::initializer_list<std::string> &&);
}
