#pragma once

#include <queue>
#include <random>

#include "../corewar.hh"

namespace arena {
	struct WarriorInfo {
		uint8_t id;           // unique program ID
		int pin;              // P-space Identification Number
		std::queue<int> pcs;  // program counters
	};

	class CorewarArena : public corewar::ArenaInterface {
		protected:
			static uint64_t defaultSeed();

			std::mt19937_64 rne;
			std::shared_ptr<const corewar::Hill> hill;
			std::vector<corewar::Instruction> arena;
			std::map<int, std::vector<int>> pspace;
			std::vector<int> pspace0;
			std::vector<corewar::Warrior> warriors;
			std::vector<uint8_t> last_modified;
			std::vector<WarriorInfo> active_warriors;
			int completed_cycles;

		public:
			CorewarArena(corewar::HillType = corewar::HillType::R94, uint64_t = defaultSeed());
			CorewarArena(const std::shared_ptr<const corewar::Hill> &, uint64_t = defaultSeed());
			void addWarrior(const corewar::Warrior &) override;
			const corewar::Warrior &getWarrior(uint8_t) const override;
			int queueSize(uint8_t) const override;
			int queueFront(uint8_t) const override;

			int size() const override;
			int pspaceSize() const override;
			int cycle() const override;
			int maxCycles() const override;
			corewar::Tile tile(int) const override;
			int living() const override;
			uint8_t movesNext() const override;

			const corewar::Instruction &getCoreAt(int) const override;
			void setCoreAt(int, corewar::Instruction) override;
			int getPspaceAt(uint8_t, int) const override;
			void setPspaceAt(uint8_t, int, int) override;

			uint8_t lastModifiedBy(int) const override;
			std::vector<std::pair<uint8_t, int>> executedBy(int) const override;
			std::vector<int> tick() override;
			void nextRound() override;

		protected:
			void evalLDP(const WarriorInfo &, int &, int);
			void evalSTP(const WarriorInfo &, int, int);
			void setupArena();
	};
}
