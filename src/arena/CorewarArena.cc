#include <algorithm>
#include <iostream>
#include <numeric>
#include <set>

#include "CorewarArena.hh"
#include "utils.hh"

using corewar::Operation;
using corewar::Modifier;
using corewar::AddrMode;
using corewar::Instruction;
using corewar::Warrior;
using corewar::Tile;
using corewar::ArenaError;

namespace arena {
	uint64_t CorewarArena::defaultSeed() {
		std::random_device rd;
		uint64_t seed = uint64_t(rd()) << 32 | rd();
		auto flags = std::cout.flags();
		std::cout << "seed: " << std::showbase << std::hex << seed << '\n';
		std::cout.flags(flags);
		return seed;
	}

	CorewarArena::CorewarArena(corewar::HillType type, uint64_t seed)
			: CorewarArena(corewar::Hills[static_cast<int>(type)], seed) {}

	CorewarArena::CorewarArena(const std::shared_ptr<const corewar::Hill> &Hill, uint64_t seed) try
			: rne(seed),
			  hill(Hill),
			  arena(hill->at("CORESIZE")),
			  pspace0(hill->at("WARRIORS"), hill->at("CORESIZE") - 1),
			  last_modified(size(), 0),
			  completed_cycles(0) {
		warriors.reserve(hill->at("WARRIORS"));
		active_warriors.reserve(hill->at("WARRIORS"));
	} catch (const std::out_of_range &) {
		throwOnMisssingKey(*Hill, {"CORESIZE", "WARRIORS"});
	}

	void CorewarArena::addWarrior(const Warrior &Warrior) try {
		if (int(warriors.size()) == hill->at("WARRIORS"))
			throw ArenaError("already added all warriors");
		if (int(Warrior.size()) > hill->at("MAXLENGTH"))
			throw ArenaError("warrior has too many instructions");
		warriors.push_back(Warrior);
		if (int(warriors.size()) == hill->at("WARRIORS"))
			setupArena();
	} catch (const std::out_of_range &) {
		throwOnMisssingKey(*hill, {"MAXLENGTH", "MINDISTANCE", "PSPACESIZE"});
	}

	const Warrior &CorewarArena::getWarrior(uint8_t id) const {
		return warriors.at(id - 1);
	}

	int CorewarArena::queueSize(uint8_t id) const {
		if (!id || id > warriors.size())
			throw std::out_of_range("0 < id <= " + std::to_string(warriors.size()));
		for (const auto &W : active_warriors)
			if (W.id == id)
				return W.pcs.size();
		return 0;
	}

	int CorewarArena::queueFront(uint8_t id) const {
		if (!id || id > warriors.size())
			throw std::out_of_range("0 < id <= " + std::to_string(warriors.size()));
		for (const auto &W : active_warriors)
			if (W.id == id)
				return W.pcs.front();
		throw ArenaError("warrior is not alive");
	}

	int CorewarArena::size() const {
		return arena.size();
	}

	int CorewarArena::pspaceSize() const {
		return hill->at("PSPACESIZE");
	}

	int CorewarArena::cycle() const {
		return completed_cycles;
	}

	int CorewarArena::maxCycles() const {
		try {
			return hill->at("MAXCYCLES");
		} catch (const std::out_of_range &) {
			return std::numeric_limits<int>::max();
		}
	}

	Tile CorewarArena::tile(int offset) const {
		switch (arena.at(offset).opcode.operation) {
			case Operation::DAT:
				return Tile::DATA;
			case Operation::SPL:
				return Tile::SPLIT;
			case Operation::JMP:
			case Operation::JMZ:
			case Operation::JMN:
			case Operation::DJN:
				return Tile::JUMP;
			default:
				return Tile::CODE;
		}
	}

	int CorewarArena::living() const {
		return active_warriors.size();
	}

	uint8_t CorewarArena::movesNext() const {
		return active_warriors.size() ? active_warriors.front().id : 0;
	}

	const Instruction &CorewarArena::getCoreAt(int offset) const {
		return arena.at(offset);
	}

	void CorewarArena::setCoreAt(int offset, Instruction instruction) {
		if ((instruction.fieldA %= size()) < 0)
			instruction.fieldA += size();
		if ((instruction.fieldB %= size()) < 0)
			instruction.fieldB += size();
		arena.at(offset) = instruction;
	}

	int CorewarArena::getPspaceAt(uint8_t id, int offset) const {
		int pin = warriors.at(id - 1).pin;
		return offset
				? pspace.at(pin < 0 ? -id : pin).at(offset)
				: pspace0.at(id - 1);
	}

	void CorewarArena::setPspaceAt(uint8_t id, int offset, int value) {
		if ((value %= size()) < 0)
			value += size();
		int pin = warriors.at(id - 1).pin;
		(offset ? pspace.at(pin < 0 ? -id : pin).at(offset) : pspace0.at(id - 1))
			= value;
	}

	uint8_t CorewarArena::lastModifiedBy(int offset) const {
		return last_modified.at(offset);
	}

	std::vector<std::pair<uint8_t, int>> CorewarArena::executedBy(int offset) const {
		if (offset < 0 || offset >= size())
			throw std::out_of_range("0 <= offset < " + std::to_string(size()));
		std::vector<std::pair<uint8_t, int>> vec;
		for (const auto &WI : active_warriors)
			for (std::queue<int> q(WI.pcs); !q.empty(); q.pop())
				if (q.front() == offset) {
					if (vec.size() && vec.back().first == WI.id)
						++vec.back().second;
					else
						vec.emplace_back(WI.id, 1);
				}
		return vec;
	}

	std::vector<int> CorewarArena::tick() {
		if (int(warriors.size()) < hill->at("WARRIORS"))
			throw ArenaError("you must add all warriors first");

		std::set<int> offsets;

		for (auto &warrior : active_warriors) {
			auto pc = warrior.pcs.front();
			warrior.pcs.pop();
			offsets.insert(pc);
			const Instruction I = arena[pc];

			int directA = (pc + I.fieldA) % size();
			int directB = (pc + I.fieldB) % size();
			if (I.opcode.addrModeA == AddrMode::A_IND_PRE) {
				arena[directA].fieldA = (arena[directA].fieldA + size() - 1) % size();
				offsets.insert(directA);
			} else if (I.opcode.addrModeA == AddrMode::B_IND_PRE) {
				arena[directA].fieldB = (arena[directA].fieldB + size() - 1) % size();
				offsets.insert(directA);
			}
			if (I.opcode.addrModeB == AddrMode::A_IND_PRE) {
				arena[directB].fieldA = (arena[directB].fieldA + size() - 1) % size();
				offsets.insert(directB);
			} else if (I.opcode.addrModeB == AddrMode::B_IND_PRE) {
				arena[directB].fieldB = (arena[directB].fieldB + size() - 1) % size();
				offsets.insert(directB);
			}

			const Instruction *Src;
			Instruction *dest;
			switch (I.opcode.addrModeA) {
				case AddrMode::IMM:
					Src = &arena[pc];
					break;
				case AddrMode::DIR:
					Src = &arena[directA];
					break;
				case AddrMode::A_IND:
				case AddrMode::A_IND_PRE:
				case AddrMode::A_IND_POS:
					Src = &arena[(directA + arena[directA].fieldA) % size()];
					break;
				case AddrMode::B_IND:
				case AddrMode::B_IND_PRE:
				case AddrMode::B_IND_POS:
					Src = &arena[(directA + arena[directA].fieldB) % size()];
					break;
			}
			switch (I.opcode.addrModeB) {
				case AddrMode::IMM:
					dest = &arena[pc];
					break;
				case AddrMode::DIR:
					dest = &arena[directB];
					break;
				case AddrMode::A_IND:
				case AddrMode::A_IND_PRE:
				case AddrMode::A_IND_POS:
					dest = &arena[(directB + arena[directB].fieldA) % size()];
					break;
				case AddrMode::B_IND:
				case AddrMode::B_IND_PRE:
				case AddrMode::B_IND_POS:
					dest = &arena[(directB + arena[directB].fieldB) % size()];
					break;
			}

			auto modified_dest = [&]() {
				last_modified[dest - &arena[0]] = warrior.id;
				offsets.insert(dest - &arena[0]);
			};
			auto add_next_instruction = [&]() {
				warrior.pcs.push((pc + 1) % size());
				offsets.insert((pc + 1) % size());
			};
			auto add_instruction = [&](int offset) {
				warrior.pcs.push(offset);
				offsets.insert(offset);
			};
			switch (I.opcode.operation) {
				case Operation::DAT:
					break;
				case Operation::MOV:
					switch (I.opcode.modifier) {
						case Modifier::A:
							dest->fieldA = Src->fieldA;
							break;
						case Modifier::B:
							dest->fieldB = Src->fieldB;
							break;
						case Modifier::AB:
							dest->fieldB = Src->fieldA;
							break;
						case Modifier::BA:
							dest->fieldA = Src->fieldB;
							break;
						case Modifier::F:
							dest->fieldA = Src->fieldA;
							dest->fieldB = Src->fieldB;
							break;
						case Modifier::X:
							dest->fieldA = Src->fieldB;
							dest->fieldB = Src->fieldA;
							break;
						case Modifier::I:
							*dest = *Src;
							break;
					}
					modified_dest();
					add_next_instruction();
					break;
				case Operation::ADD:
					switch (I.opcode.modifier) {
						case Modifier::A:
							dest->fieldA = (dest->fieldA + Src->fieldA) % size();
							break;
						case Modifier::B:
							dest->fieldB = (dest->fieldB + Src->fieldB) % size();
							break;
						case Modifier::AB:
							dest->fieldB = (dest->fieldB + Src->fieldA) % size();
							break;
						case Modifier::BA:
							dest->fieldA = (dest->fieldA + Src->fieldB) % size();
							break;
						case Modifier::F:
						case Modifier::I:
							dest->fieldA = (dest->fieldA + Src->fieldA) % size();
							dest->fieldB = (dest->fieldB + Src->fieldB) % size();
							break;
						case Modifier::X:
							dest->fieldA = (dest->fieldA + Src->fieldB) % size();
							dest->fieldB = (dest->fieldB + Src->fieldA) % size();
							break;
					}
					modified_dest();
					add_next_instruction();
					break;
				case Operation::SUB:
					switch (I.opcode.modifier) {
						case Modifier::A:
							dest->fieldA = (dest->fieldA + size() - Src->fieldA) % size();
							break;
						case Modifier::B:
							dest->fieldB = (dest->fieldB + size() - Src->fieldB) % size();
							break;
						case Modifier::AB:
							dest->fieldB = (dest->fieldB + size() - Src->fieldA) % size();
							break;
						case Modifier::BA:
							dest->fieldA = (dest->fieldA + size() - Src->fieldB) % size();
							break;
						case Modifier::F:
						case Modifier::I:
							dest->fieldA = (dest->fieldA + size() - Src->fieldA) % size();
							dest->fieldB = (dest->fieldB + size() - Src->fieldB) % size();
							break;
						case Modifier::X:
							dest->fieldA = (dest->fieldA + size() - Src->fieldB) % size();
							dest->fieldB = (dest->fieldB + size() - Src->fieldA) % size();
							break;
					}
					modified_dest();
					add_next_instruction();
					break;
				case Operation::MUL:
					switch (I.opcode.modifier) {
						case Modifier::A:
							dest->fieldA = (dest->fieldA * Src->fieldA) % size();
							break;
						case Modifier::B:
							dest->fieldB = (dest->fieldB * Src->fieldB) % size();
							break;
						case Modifier::AB:
							dest->fieldB = (dest->fieldB * Src->fieldA) % size();
							break;
						case Modifier::BA:
							dest->fieldA = (dest->fieldA * Src->fieldB) % size();
							break;
						case Modifier::F:
						case Modifier::I:
							dest->fieldA = (dest->fieldA * Src->fieldA) % size();
							dest->fieldB = (dest->fieldB * Src->fieldB) % size();
							break;
						case Modifier::X:
							dest->fieldA = (dest->fieldA * Src->fieldB) % size();
							dest->fieldB = (dest->fieldB * Src->fieldA) % size();
							break;
					}
					modified_dest();
					add_next_instruction();
					break;
				case Operation::DIV:
					switch (I.opcode.modifier) {
						case Modifier::A:
							if (Src->fieldA != 0) {
								dest->fieldA = (dest->fieldA / Src->fieldA) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::B:
							if (Src->fieldB != 0) {
								dest->fieldB = (dest->fieldB / Src->fieldB) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::AB:
							if (Src->fieldA != 0) {
								dest->fieldB = (dest->fieldB / Src->fieldA) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::BA:
							if (Src->fieldB != 0) {
								dest->fieldA = (dest->fieldA / Src->fieldB) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::F:
						case Modifier::I:
							if (Src->fieldA != 0) {
								dest->fieldA = (dest->fieldA / Src->fieldA) % size();
								modified_dest();
							}
							if (Src->fieldB != 0) {
								dest->fieldB = (dest->fieldB / Src->fieldB) % size();
								modified_dest();
							}
							if (Src->fieldA != 0 && Src->fieldB != 0)
								add_next_instruction();
							break;
						case Modifier::X:
							if (Src->fieldB != 0) {
								dest->fieldA = (dest->fieldA / Src->fieldB) % size();
								modified_dest();
							}
							if (Src->fieldA != 0) {
								dest->fieldB = (dest->fieldB / Src->fieldA) % size();
								modified_dest();
							}
							if (Src->fieldA != 0 && Src->fieldB != 0)
								add_next_instruction();
							break;
					}
					break;
				case Operation::MOD:
					switch (I.opcode.modifier) {
						case Modifier::A:
							if (Src->fieldA != 0) {
								dest->fieldA = (dest->fieldA % Src->fieldA) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::B:
							if (Src->fieldB != 0) {
								dest->fieldB = (dest->fieldB % Src->fieldB) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::AB:
							if (Src->fieldA != 0) {
								dest->fieldB = (dest->fieldB % Src->fieldA) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::BA:
							if (Src->fieldB != 0) {
								dest->fieldA = (dest->fieldA % Src->fieldB) % size();
								modified_dest();
								add_next_instruction();
							}
							break;
						case Modifier::F:
						case Modifier::I:
							if (Src->fieldA != 0) {
								dest->fieldA = (dest->fieldA % Src->fieldA) % size();
								modified_dest();
							}
							if (Src->fieldB != 0) {
								dest->fieldB = (dest->fieldB % Src->fieldB) % size();
								modified_dest();
							}
							if (Src->fieldA != 0 && Src->fieldB != 0)
								add_next_instruction();
							break;
						case Modifier::X:
							if (Src->fieldB != 0) {
								dest->fieldA = (dest->fieldA % Src->fieldB) % size();
								modified_dest();
							}
							if (Src->fieldA != 0) {
								dest->fieldB = (dest->fieldB % Src->fieldA) % size();
								modified_dest();
							}
							if (Src->fieldA != 0 && Src->fieldB != 0)
								add_next_instruction();
							break;
					}
					break;
				case Operation::JMP:
eval_jmp:
					add_instruction(Src - &arena[0]);
					break;
				case Operation::JMZ:
					switch (I.opcode.modifier) {
						case Modifier::A:
						case Modifier::BA:
							if (dest->fieldA == 0)
								goto eval_jmp;
							break;
						case Modifier::B:
						case Modifier::AB:
							if (dest->fieldB == 0)
								goto eval_jmp;
							break;
						case Modifier::F:
						case Modifier::X:
						case Modifier::I:
							if (dest->fieldA == 0 && dest->fieldB == 0)
								goto eval_jmp;
							break;
					}
					add_next_instruction();
					break;
				case Operation::JMN:
eval_jmn:
					switch (I.opcode.modifier) {
						case Modifier::A:
						case Modifier::BA:
							if (dest->fieldA != 0)
								goto eval_jmp;
							break;
						case Modifier::B:
						case Modifier::AB:
							if (dest->fieldB != 0)
								goto eval_jmp;
							break;
						case Modifier::F:
						case Modifier::X:
						case Modifier::I:
							if (dest->fieldA != 0 || dest->fieldB != 0)
								goto eval_jmp;
							break;
					}
					add_next_instruction();
					break;
				case Operation::DJN:
					switch (I.opcode.modifier) {
						case Modifier::A:
						case Modifier::BA:
							dest->fieldA = (dest->fieldA + size() - 1) % size();
							break;
						case Modifier::B:
						case Modifier::AB:
							dest->fieldB = (dest->fieldB + size() - 1) % size();
							break;
						case Modifier::F:
						case Modifier::X:
						case Modifier::I:
							dest->fieldA = (dest->fieldA + size() - 1) % size();
							dest->fieldB = (dest->fieldB + size() - 1) % size();
							break;
					}
					modified_dest();
					goto eval_jmn;
				case Operation::SPL:
					add_next_instruction();
					{
						auto It = hill->find("MAXPROCESSES");
						if (It != hill->end() && int(warrior.pcs.size()) < It->second)
							add_instruction(Src - &arena[0]);
					}
					break;
				case Operation::CMP:
				case Operation::SEQ:
					switch (I.opcode.modifier) {
						case Modifier::A:
							add_instruction((pc + 1 + (dest->fieldA == Src->fieldA)) % size());
							break;
						case Modifier::B:
							add_instruction((pc + 1 + (dest->fieldB == Src->fieldB)) % size());
							break;
						case Modifier::AB:
							add_instruction((pc + 1 + (dest->fieldB == Src->fieldA)) % size());
							break;
						case Modifier::BA:
							add_instruction((pc + 1 + (dest->fieldA == Src->fieldB)) % size());
							break;
						case Modifier::F:
							add_instruction((pc + 1 + (dest->fieldA == Src->fieldA && dest->fieldB == Src->fieldB)) % size());
							break;
						case Modifier::X:
							add_instruction((pc + 1 + (dest->fieldA == Src->fieldB && dest->fieldB == Src->fieldA)) % size());
							break;
						case Modifier::I:
							add_instruction((pc + 1 + (*dest == *Src)) % size());
							break;
					}
					break;
				case Operation::SNE:
					switch (I.opcode.modifier) {
						case Modifier::A:
							add_instruction((pc + 1 + (dest->fieldA != Src->fieldA)) % size());
							break;
						case Modifier::B:
							add_instruction((pc + 1 + (dest->fieldB != Src->fieldB)) % size());
							break;
						case Modifier::AB:
							add_instruction((pc + 1 + (dest->fieldB != Src->fieldA)) % size());
							break;
						case Modifier::BA:
							add_instruction((pc + 1 + (dest->fieldA != Src->fieldB)) % size());
							break;
						case Modifier::F:
							add_instruction((pc + 1 + (dest->fieldA != Src->fieldA && dest->fieldB != Src->fieldB)) % size());
							break;
						case Modifier::X:
							add_instruction((pc + 1 + (dest->fieldA != Src->fieldB && dest->fieldB != Src->fieldA)) % size());
							break;
						case Modifier::I:
							add_instruction((pc + 1 + (*dest != *Src)) % size());
							break;
					}
					break;
				case Operation::SLT:
					switch (I.opcode.modifier) {
						case Modifier::A:
							add_instruction((pc + 1 + (Src->fieldA < dest->fieldA)) % size());
							break;
						case Modifier::B:
							add_instruction((pc + 1 + (Src->fieldB < dest->fieldB)) % size());
							break;
						case Modifier::AB:
							add_instruction((pc + 1 + (Src->fieldB < dest->fieldA)) % size());
							break;
						case Modifier::BA:
							add_instruction((pc + 1 + (Src->fieldA < dest->fieldB)) % size());
							break;
						case Modifier::F:
						case Modifier::I:
							add_instruction((pc + 1 + (Src->fieldA < dest->fieldA && Src->fieldB < dest->fieldB)) % size());
							break;
						case Modifier::X:
							add_instruction((pc + 1 + (Src->fieldA < dest->fieldB && Src->fieldB < dest->fieldA)) % size());
							break;
					}
					break;
				case Operation::LDP:
					switch (I.opcode.modifier) {
						case Modifier::A:
							evalLDP(warrior, dest->fieldA, Src->fieldA);
							break;
						case Modifier::AB:
							evalLDP(warrior, dest->fieldB, Src->fieldA);
							break;
						case Modifier::BA:
							evalLDP(warrior, dest->fieldA, Src->fieldB);
							break;
						case Modifier::B:
						case Modifier::F:
						case Modifier::X:
						case Modifier::I:
							evalLDP(warrior, dest->fieldB, Src->fieldB);
							break;
					}
					modified_dest();
					add_next_instruction();
					break;
				case Operation::STP:
					switch (I.opcode.modifier) {
						case Modifier::A:
							evalSTP(warrior, Src->fieldA, dest->fieldA);
							break;
						case Modifier::AB:
							evalSTP(warrior, Src->fieldA, dest->fieldB);
							break;
						case Modifier::BA:
							evalSTP(warrior, Src->fieldB, dest->fieldA);
							break;
						case Modifier::B:
						case Modifier::F:
						case Modifier::X:
						case Modifier::I:
							evalSTP(warrior, Src->fieldB, dest->fieldB);
							break;
					}
					add_next_instruction();
					break;
				case Operation::NOP:
					add_next_instruction();
					break;
			}

			if (I.opcode.addrModeA == AddrMode::A_IND_POS) {
				arena[directA].fieldA = (arena[directA].fieldA + 1) % size();
				offsets.insert(directA);
			} else if (I.opcode.addrModeA == AddrMode::B_IND_POS) {
				arena[directA].fieldB = (arena[directA].fieldB + 1) % size();
				offsets.insert(directA);
			}
			if (I.opcode.addrModeB == AddrMode::A_IND_POS) {
				arena[directB].fieldA = (arena[directB].fieldA + 1) % size();
				offsets.insert(directB);
			} else if (I.opcode.addrModeB == AddrMode::B_IND_POS) {
				arena[directB].fieldB = (arena[directB].fieldB + 1) % size();
				offsets.insert(directB);
			}
		}

		++completed_cycles;
		for (auto it = active_warriors.begin(); it != active_warriors.end();)
			it->pcs.empty() ? it = active_warriors.erase(it) : ++it;

		std::vector<int> need_update;
		need_update.reserve(offsets.size());
		for (int offset : offsets)
			need_update.push_back(offset);

		return need_update;
	}

	void CorewarArena::nextRound() {
		std::fill(arena.begin(), arena.end(), Instruction());
		std::fill(last_modified.begin(), last_modified.end(), 0);
		std::fill(pspace0.begin(), pspace0.end(), 0);
		for (const auto &WI : active_warriors)
			pspace0[WI.id - 1] = int(active_warriors.size()) % size();
		active_warriors.clear();
		completed_cycles = 0;
		setupArena();
	}

	void CorewarArena::evalLDP(const WarriorInfo &WI, int &field, int offset) {
		if (!offset)
			field = pspace0[WI.id - 1];
		else if (pspaceSize())
			field = pspace[WI.pin][offset % pspace[WI.pin].size()];
	}

	void CorewarArena::evalSTP(const WarriorInfo &WI, int value, int offset) {
		if (offset && pspaceSize())
			pspace[WI.pin][offset % pspace[WI.pin].size()] = value;
	}

	void CorewarArena::setupArena() {
		int to_fill = arena.size() - warriors.size() * hill->at("MINDISTANCE")
				- std::accumulate(warriors.begin(), warriors.end(), 0,
						[](int init, const Warrior &W) { return init + W.size(); });
		if (to_fill < 0)
			throw ArenaError("warriors won\'t fit in the core");

		std::vector<int> distances(warriors.size());
		fillWithNumbersSummingTo(distances, to_fill, rne);

		std::vector<uint8_t> permutation(warriors.size());
		std::iota(permutation.begin(), permutation.end(), 1);
		std::shuffle(permutation.begin(), permutation.end(), rne);

		int offset = 0;
		for (uint8_t id : permutation) {
			const Warrior &W = warriors[id - 1];
			active_warriors.push_back(
					{id, W.pin < 0 ? -id : W.pin, std::queue<int>({offset + W.origin})});
			for (int j = 0; j < int(W.size()); ++j) {
				arena[offset + j] = W[j];
				last_modified[offset + j] = id;
			}
			if (!pspace.count(active_warriors.back().pin))
				pspace[active_warriors.back().pin] = std::vector<int>(pspaceSize());
			offset += W.size() + hill->at("MINDISTANCE") + distances[id - 1];
		}
	}
}
