%class-header RedcodeParser.hh
%implementation-header RedcodeParser.ihh
%baseclass-header RedcodeParserBase.hh
%baseclass-preinclude utils.hh
%scanner RedcodeScanner.hh
%namespace parser
%class-name RedcodeParser
%scanner-class-name RedcodeScanner
%token OPERATION NUMBER NAME EQU FOR ROF ORG END PIN ASSERT
%right '='
%left OR
%left AND
%left '<' '>' NE EQ LE GE
%left '-' '+'
%left '*' '/' '%'
%right '!'
%polymorphic STR: std::string; LST: std::list<std::string>; EXP: Expression *; INT: int
%type <STR> operation address addr_mode name label token flawed_token for_token equ_token
%type <LST> labels for_body equ_line equ_stmt
%type <EXP> expression
%%
redcode : program
        | program end_stmt any_tokens
        ;
program : program instruction
        | program labels '\n'            { addLabels(environments.back(), $2, instr_num); }
        | program label                  { _$$.assign<Tag_::INT>(d_scanner.prevLineNr()); }
                        equ_stmt         { if (d_scanner.equates.count($2)) {
                                             // 2 consecutive EQUs of same label were encountered
                                             error_line_num = _$3.get<Tag_::INT>();
                                             throw corewar::AssemblerError("redefinition of label \"" + $2 + "\"");
                                           }
                                           d_scanner.equates[$2] = expandEqu($4);
                                           if (token_() == NAME && d_scanner.matched() == $2)
                                             // lookahead token is this EQU's label
                                             d_scanner.pushEqu(d_scanner.equates.at($2)); }
        | program for_stmt
        | program ASSERT expression '\n' { expressions.push_back($3);
                                           environments.emplace_back(environments.back(), instr_num);
                                           $3->solve(environments.back());
                                           tree.addAssertion($3); }
        | program ORG expression '\n'    { expressions.push_back($3);
                                           environments.emplace_back(environments.back(), 0);
                                           $3->solve(environments.back());
                                           tree.setOrigin($3); }
        | program PIN expression '\n'    { expressions.push_back($3);
                                           environments.emplace_back(environments.back(), 0);
                                           $3->solve(environments.back());
                                           tree.setPIN($3); }
        |
        ;
instruction : labels operation address ',' address '\n' { addLabels(environments.back(), $1, instr_num++);
                                                          tree.addNode(SyntaxTree::Node($2, $3, *----expressions.end(), $5, expressions.back())); }
            | labels operation address '\n'             { addLabels(environments.back(), $1, instr_num++);
                                                          tree.addNode(SyntaxTree::Node($2, $3, expressions.back())); }
            ;
operation : OPERATION { std::string s(d_scanner.matched());
                        std::transform(s.begin(), s.end(), s.begin(), toupper);
                        $$(s); }
          ;
address : addr_mode expression { expressions.push_back($2);
                                 environments.emplace_back(environments.back(), instr_num);
                                 $2->solve(environments.back());
                                 $$($1); }
        | expression           { expressions.push_back($1);
                                 environments.emplace_back(environments.back(), instr_num);
                                 $1->solve(environments.back());
                                 $$(""); }
        ;
addr_mode : '#' { $$("#"); }
          | '$' { $$("$"); }
          | '*' { $$("*"); }
          | '@' { $$("@"); }
          | '{' { $$("{"); }
          | '<' { $$("<"); }
          | '}' { $$("}"); }
          | '>' { $$(">"); }
          ;
expression : expression '*' expression { $$(new Expression('*', $1, $3)); }
           | expression '/' expression { $$(new Expression('/', $1, $3)); }
           | expression '%' expression { $$(new Expression('%', $1, $3)); }
           | expression '-' expression { $$(new Expression('-', $1, $3)); }
           | expression '+' expression { $$(new Expression('+', $1, $3)); }
           | expression '<' expression { $$(new Expression('<', $1, $3)); }
           | expression '>' expression { $$(new Expression('>', $1, $3)); }
           | expression NE expression  { $$(new Expression('!' << 8 | '=', $1, $3)); }
           | expression EQ expression  { $$(new Expression('=' << 8 | '=', $1, $3)); }
           | expression LE expression  { $$(new Expression('<' << 8 | '=', $1, $3)); }
           | expression GE expression  { $$(new Expression('>' << 8 | '=', $1, $3)); }
           | expression AND expression { $$(new Expression('&' << 8 | '&', $1, $3)); }
           | expression OR expression  { $$(new Expression('|' << 8 | '|', $1, $3)); }
           | expression '=' expression { $$(new Expression('=', $1, $3)); }
           | '-' expression %prec '!'  { $$(new Expression('-', $2)); }
           | '+' expression %prec '!'  { $$(new Expression('+', $2)); }
           | '!' expression            { $$(new Expression('!', $2)); }
           | '(' expression ')'        { $$($2); }
           | NUMBER                    { $$(new Expression(std::stoi(d_scanner.matched(), 0, std::isalpha(d_scanner.matched()[1]) ? 16 : 10))); }
           | name                      { $$(new Expression($1)); }
           ;
labels : label labels { $2.push_front($1);
                        $$($2); }
       |              { $$(); }
       ;
label : name ':' { $$($1); }
      | name     { $$($1); }
      ;
name : NAME { $$(d_scanner.matched()); }
     ;
any_tokens : any_tokens any_token
           |
           ;
any_token : for_token | FOR | ROF
          ;
end_stmt : labels END '\n'            { addLabels(environments.back(), $1, instr_num); }
         | labels END expression '\n' { addLabels(environments.back(), $1, instr_num);
                                        expressions.push_back($3);
                                        environments.emplace_back(environments.back(), 0);
                                        $3->solve(environments.back());
                                        tree.setOrigin($3); }
         ;
equ_stmt : EQU equ_line '\n'          { $$($2); }
         | equ_stmt EQU equ_line '\n' { $1.push_back("\n");
                                        $1.splice($1.end(), $3);
                                        $$($1); }
         ;
equ_line : equ_token          { $$(std::list<std::string>(1, $1)); }
         | equ_line equ_token { $1.push_back($2);
                                $$($1); }
         ;
equ_token : token { $$($1); }
          | FOR   { $$(d_scanner.matched()); }
          | ROF   { $$(d_scanner.matched()); }
          ;
for_stmt : labels FOR expression '\n'                   { _$$.assign<Tag_::INT>(d_scanner.prevLineNr()); }
                                      for_body ROF '\n' { if ($1.size()) {
                                                            auto last = $1.back();
                                                            $1.pop_back();
                                                            addLabels(environments.back(), $1, instr_num);
                                                            $1.push_back(last);
                                                          }
                                                          expressions.push_back($3);
                                                          environments.emplace_back(environments.back(), instr_num);
                                                          if (!$3->solve(environments.back())
                                                              && !solveAll(expressions, environments)) {
                                                            error_line_num = _$5.get<Tag_::INT>();
                                                            throw corewar::AssemblerError("cannot unwind FOR; unknown value of expression");
                                                          }
                                                          d_scanner.pushFor($1.empty()
                                                              ? expandForRof(expressions.back()->getValue(), $6)
                                                              : expandForRof(expressions.back()->getValue(), $6, $1.back())); }
         ;
for_body : FOR for_body ROF '\n' for_body { $5.push_front("\n");
                                            $5.push_front("ROF");
                                            $5.splice($5.begin(), $2);
                                            $5.push_front("FOR");
                                            $$($5); }
         | for_token for_body             { $2.push_front($1);
                                            $$($2); }
         |                                { $$(); }
         ;
for_token : token        { $$($1); }
          | '\n'         { $$("\n"); }
          | flawed_token { $$($1); }
          ;
flawed_token : '.'  { $$("."); }
             | '\'' { $$("\'"); }
             | '\"' { $$("\""); }
             | '['  { $$("["); }
             | ']'  { $$("]"); }
             | '?'  { $$("?"); }
             | '~'  { $$("~"); }
             ;
token : ASSERT    { $$(";assert"); }
      | OPERATION { $$(d_scanner.matched()); }
      | addr_mode { $$($1); }
      | NUMBER    { $$(d_scanner.matched()); }
      | name      { $$($1); }
      | EQU       { $$(d_scanner.matched()); }
      | ORG       { $$(d_scanner.matched()); }
      | END       { $$(d_scanner.matched()); }
      | ','       { $$(","); }
      | '&'       { $$("&"); }
      | ':'       { $$(":"); }
      | '='       { $$("="); }
      | OR        { $$(d_scanner.matched()); }
      | AND       { $$(d_scanner.matched()); }
      | NE        { $$(d_scanner.matched()); }
      | EQ        { $$(d_scanner.matched()); }
      | LE        { $$(d_scanner.matched()); }
      | GE        { $$(d_scanner.matched()); }
      | '-'       { $$("-"); }
      | '+'       { $$("+"); }
      | '/'       { $$("/"); }
      | '%'       { $$("%"); }
      | '('       { $$("("); }
      | ')'       { $$(")"); }
      | '!'       { $$("!"); }
      ;
