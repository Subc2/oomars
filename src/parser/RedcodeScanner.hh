#pragma once

#include <forward_list>

#include "RedcodeScannerBase.hh"
#include "RedcodeParserBase.hh"

using corewar::CommentType;

namespace parser {
	struct LineNumberController {
		int line_num;
		int prev_token;
		int tokens_to_ignore;
		std::forward_list<std::array<int, 3>> line_deduction;
	};

class RedcodeScanner : public RedcodeScannerBase {  // Bisonc++ fix (no indent)
		public:
			std::map<CommentType, std::string> comments;
			std::map<std::string, std::tuple<std::string, int>> equates;

		private:
			LineNumberController lnc;

		public:
			explicit RedcodeScanner(std::istream & = std::cin, std::ostream & = std::cout);
			RedcodeScanner(std::string const &, std::string const &);

			int lex();
			int prevLineNr() const;
			void pushEqu(const std::tuple<std::string, int> &);
			void pushFor(const std::tuple<std::string, int, int, int> &);

		private:
			int lex_();
			int executeAction_(size_t);
			void print();
			void preCode();
			void postCode(PostEnum_);
	};
}
