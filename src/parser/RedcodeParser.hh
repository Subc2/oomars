#pragma once

#include <algorithm>

#include "RedcodeParserBase.hh"
#include "RedcodeScanner.hh"
#include "../corewar.hh"

#undef RedcodeParser

namespace parser {
class RedcodeParser : public RedcodeParserBase {  // Flexc++ fix (no indent)
		public:
			corewar::Warrior warrior;

		private:
			RedcodeScanner d_scanner;
			int error_line_num;
			int instr_num;
			std::list<Expression *> expressions;
			std::list<Environment> environments;
			SyntaxTree tree;

		public:
			RedcodeParser(std::istream & = std::cin, std::ostream & = std::cout);
			RedcodeParser(const std::shared_ptr<const std::map<std::string, int>> &, std::istream & = std::cin, std::ostream & = std::cout);
			~RedcodeParser();

			void assemble();
			int parse();

		private:
			void error();           // called on (syntax) errors
			int lex();              // returns the next token from the lexical scanner
			void print();           // use, e.g., d_token, d_loc
			void exceptionHandler(std::exception const &);

			// support functions for parse():
			void executeAction_(int);
			void errorRecovery_();
			void nextCycle_();
			void nextToken_();
			void print_();
	};
}
