#pragma once

#include <bitset>
#include <list>

#include "../corewar.hh"

namespace parser {
	struct undefined : public std::runtime_error {
		explicit undefined(const std::string &);
	};

	struct Environment {
		protected:
			typedef std::map<std::string, int> dict_t;

			int instr_num;
			std::shared_ptr<const corewar::Hill> constants;
			std::shared_ptr<dict_t> labels;
			std::map<char, int> variables;
			std::bitset<'z' - 'a' + 1> declared;

		public:
			explicit Environment(const std::shared_ptr<const corewar::Hill> & = nullptr);
			explicit Environment(const corewar::Hill &);
			explicit Environment(corewar::Hill &&);
			Environment(const Environment &, int);
			void addLabel(const std::string &, int);
			void markVariableAsUndefined(char);
			void setInstrNum(int);
			void setVariable(char, int);
			void updateFront(const Environment &);
			int valueOf(const std::string &) const;

		protected:
			bool isDeclared(const std::string &) const;
			bool isDeclaredVariable(const std::string &) const;
	};

	struct Expression {
		protected:
			int op;
			Expression *A, *B;
			int value;
			std::string label;

		public:
			Expression(int, Expression *, Expression * = nullptr);
			explicit Expression(int);
			explicit Expression(const std::string &);
			~Expression();
			Expression &operator=(Expression &&) = delete;
			explicit operator std::string() const;
			int getValue() const;
			bool modSolve(Environment &);
			bool solve(Environment &);

		private:
			int compute(int, int);
			int compute(int, int, int);
	};

	struct SyntaxTree {
		struct Node {
			corewar::Opcode opcode;
			Expression *A, *B;

			Node(const std::string &, const std::string &, Expression *);
			Node(const std::string &, const std::string &, Expression *, const std::string &, Expression *);
		};

		Expression *origin;
		Expression *pin;
		std::list<Expression *> assertions;
		std::list<Node> nodes;

		SyntaxTree();
		void addAssertion(Expression *);
		void addNode(const Node &);
		void addNode(Node &&);
		void setOrigin(Expression *);
		void setPIN(Expression *);
	};

	std::tuple<std::string, int> expandEqu(const std::list<std::string> &);
	std::tuple<std::string, int, int, int> expandForRof(int, const std::list<std::string> &, const std::string & = "");
	void addLabels(Environment &, const std::list<std::string> &, int);
	bool solveAll(std::list<Expression *> &, std::list<Environment> &);
}
