#include <iomanip>
#include <iostream>
#include <sstream>

#include "utils.hh"

using corewar::Hill;
using corewar::AssemblerError;

namespace {
	std::string op2string(int op) {
		return op > 0xff
				? std::string({char(op >> 8 & 0xff), char(op & 0xff)})
				: std::string(1, op);
	}
}

namespace parser {
	undefined::undefined(const std::string &what_arg)
			: std::runtime_error(what_arg) {}


	Environment::Environment(const std::shared_ptr<const Hill> &Constants)
			: instr_num(-1),
			  constants(Constants ? Constants : std::make_shared<Hill>()),
			  labels(std::make_shared<dict_t>()) {}

	Environment::Environment(const Hill &Constants)
			: instr_num(-1),
			  constants(std::make_shared<Hill>(Constants)),
			  labels(std::make_shared<dict_t>()) {}

	Environment::Environment(Hill &&constants)
			: instr_num(-1),
			  constants(std::make_shared<Hill>(std::move(constants))),
			  labels(std::make_shared<dict_t>()) {}

	Environment::Environment(const Environment &Env, int instrNum)
			: instr_num(instrNum),
			  constants(Env.constants),
			  labels(Env.labels),
			  variables(Env.variables),
			  declared(Env.declared) {}

	void Environment::addLabel(const std::string &key, int value) {
		if (isDeclared(key))
			throw AssemblerError("\"" + key + "\" already declared");
		(*labels)[key] = value;
	}

	void Environment::markVariableAsUndefined(char key) {
		declared.set(std::tolower(key) - 'a');
		variables.erase(std::tolower(key));
	}

	void Environment::setInstrNum(int value) {
		instr_num = value;
	}

	void Environment::setVariable(char key, int value) {
		declared.set(std::tolower(key) - 'a');
		variables[key] = value;
	}

	// Expression::solve(Environment &) should be run afterwards
	void Environment::updateFront(const Environment &Env) {
		for (const auto &Pair : Env.variables)
			if (!variables.count(Pair.first))
				setVariable(Pair.first, Pair.second);
	}

	int Environment::valueOf(const std::string &key) const {
		dict_t::const_iterator it;
		if ((it = constants->find(key)) != constants->end())
			return it->second;
		if ((it = labels->find(key)) != labels->end())
			return it->second - instr_num;
		if (key == "CURLINE")
			return instr_num;
		if (isDeclaredVariable(key))
			try {
				return variables.at(std::tolower(key[0]));
			} catch (const std::out_of_range &) {
				throw undefined("variable \'" + key + "\' is undefined");
			}
		throw std::out_of_range("\"" + key + "\" is not declared");
	}

	bool Environment::isDeclared(const std::string &key) const {
		return constants->count(key)
				|| labels->count(key)
				|| key == "CURLINE"
				|| isDeclaredVariable(key);
	}

	bool Environment::isDeclaredVariable(const std::string &key) const {
		return key.length() == 1
				&& std::isalpha(key[0])
				&& declared[std::tolower(key[0]) - 'a'];
	}


	Expression::Expression(int op, Expression *A, Expression *B)
			: op(op), A(A), B(B) {}

	Expression::Expression(int value)
			: op(0), A(nullptr), B(nullptr), value(value) {}

	Expression::Expression(const std::string &Label)
			: op(1), A(nullptr), B(nullptr), label(Label) {}

	Expression::~Expression() {
		delete A;
		delete B;
	}

	Expression::operator std::string() const {
		switch (op) {
			case 0:
				return std::to_string(value);
			case 1:
				return label;
		}
		return B
				? "(" + std::string(*A) + " " + op2string(op) + " " + std::string(*B) + ")"
				: op2string(op) + std::string(*A);
	}

	int Expression::getValue() const {
		if (op)
			throw AssemblerError(
					"expression \"" + std::string(*this) + "\" has undefined value");
		return value;
	}

	bool Expression::modSolve(Environment &env) {
		try {
			int coresize = env.valueOf("CORESIZE");
			if (!coresize)
				return false;
			if ((value %= coresize) < 0)
				value += coresize;
			return true;
		} catch (...) {
		}
		return false;
	}

	bool Expression::solve(Environment &env) {
		if (!B)
			switch (op) {
				case '-':
				case '+':
				case '!':
					if (A->solve(env)) {
						value = compute(op, A->value);
						delete std::exchange(A, nullptr);
						return !(op = 0);
					}
					return false;
			}
		switch (op) {
			case 0:
				return true;
			case 1:
				try {
					value = env.valueOf(label);
					return !(op = 0);
				} catch (...) {
				}
				break;
			case '=':
				if (A->op != 1 || A->label.size() != 1 || !std::isalpha(A->label[0]))
					throw AssemblerError(
							"cannot initialize variable \"" + std::string(*A) + "\"");
				if (B->solve(env)) {
					env.setVariable(A->label[0], value = B->value);
					delete std::exchange(A, nullptr);
					delete std::exchange(B, nullptr);
					return !(op = 0);
				}
				env.markVariableAsUndefined(A->label[0]);
				break;
			default:
			{
				bool first = A->solve(env);
				if (B->solve(env) && first) {
					try {
						value = compute(op, A->value, B->value);
					} catch (const std::invalid_argument &) {
						return false;
					}
					delete std::exchange(A, nullptr);
					delete std::exchange(B, nullptr);
					return !(op = 0);
				}
				break;
			}
		}
		return false;
	}

	int Expression::compute(int op, int a) {
		switch (op) {
			case '-':
				return -a;
			case '+':
				return a;
			case '!':
				return !a;
		}
		throw AssemblerError("invalid unary operator \"" + op2string(op) + "\"");
	}

	int Expression::compute(int op, int a, int b) {
		switch (op) {
			case '*':
				return a * b;
			case '/':
				if (!b)
					throw std::invalid_argument(std::to_string(a) + " / " + std::to_string(b));
				return a / b;
			case '%':
				if (!b)
					throw std::invalid_argument(std::to_string(a) + " % " + std::to_string(b));
				return a % b;
			case '-':
				return a - b;
			case '+':
				return a + b;
			case '<':
				return a < b;
			case '>':
				return a > b;
			case '!' << 8 | '=':
				return a != b;
			case '=' << 8 | '=':
				return a == b;
			case '<' << 8 | '=':
				return a <= b;
			case '>' << 8 | '=':
				return a >= b;
			case '&' << 8 | '&':
				return a && b;
			case '|' << 8 | '|':
				return a || b;
			case '=':
				return b;
		}
		throw AssemblerError("invalid binary operator \"" + op2string(op) + "\"");
	}


	SyntaxTree::Node::Node(const std::string &Opcode, const std::string &ModA, Expression *A)
			: opcode(Opcode, ModA), A(A), B(nullptr) {}

	SyntaxTree::Node::Node(const std::string &Opcode, const std::string &ModA, Expression *A, const std::string &ModB, Expression *B)
			: opcode(Opcode, ModA, ModB), A(A), B(B) {}

	SyntaxTree::SyntaxTree() : origin(nullptr), pin(nullptr) {}

	void SyntaxTree::addAssertion(Expression *expr) {
		assertions.push_back(expr);
	}

	void SyntaxTree::addNode(const Node &Node) {
		nodes.push_back(Node);
	}

	void SyntaxTree::addNode(Node &&node) {
		nodes.push_back(std::move(node));
	}

	void SyntaxTree::setOrigin(Expression *expr) {
		if (origin)
			std::cerr << "Multiple values for origin.\n";
		origin = expr;
	}

	void SyntaxTree::setPIN(Expression *expr) {
		if (pin)
			std::cerr << "Multiple values for PIN.\n";
		pin = expr;
	}


	std::tuple<std::string, int> expandEqu(const std::list<std::string> &List) {
		std::ostringstream oss;
		for (const auto &It : List)
			oss << ' ' << It;
		return std::make_tuple(oss.str(), List.size());
	}

	// returns std::string consisting of '\n'-terminated lines (e.g. "MOV 0, 1\n")
	std::tuple<std::string, int, int, int> expandForRof(int count, const std::list<std::string> &List, const std::string &Counter) {
		std::ostringstream oss;
		int newlines = 0, tokens = 0;
		oss.fill('0');
		for (int i = 1; i <= count; ++i) {
			bool join = false;
			for (const auto &It : List) {
				if (It == "&") {
					join = true;
					continue;
				}
				if (i == 1 && It == "\n")
					++newlines;
				switch (join << 1 | (It == Counter)) {
					case 0:
						oss << ' ' << It;
						++tokens;
						break;
					case 1:
						oss << ' ' << i;
						++tokens;
						break;
					case 2:
						oss << '&' << It;
						tokens += 2;
						break;
					case 3:
						oss << std::setw(2) << i;
						break;
				}
				join = false;
			}
			if (join)
				throw AssemblerError("bad join operator (&) usage");
		}
		return std::make_tuple(oss.str(), tokens, count, newlines);
	}

	void addLabels(Environment &env, const std::list<std::string> &Labels, int instrNum) {
		for (const auto &Label : Labels)
			env.addLabel(Label, instrNum);
	}

	bool solveAll(std::list<Expression *> &exps, std::list<Environment> &envs) {
		auto expsIt = exps.begin();
		auto envsIt = envs.begin();
		bool lastSolved = true;
		Environment *lastEnv = nullptr;
		for (; expsIt != exps.end(); ++expsIt, ++envsIt) {
			if (lastEnv)
				envsIt->updateFront(*lastEnv);
			lastSolved = (*expsIt)->solve(*envsIt);
			lastEnv = &*envsIt;
		}
		return lastSolved;
	}
}
