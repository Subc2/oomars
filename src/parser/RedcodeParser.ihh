#include "RedcodeParser.hh"

namespace parser {
	RedcodeParser::RedcodeParser(std::istream &in, std::ostream &out)
			: RedcodeParser(corewar::Hills[static_cast<int>(corewar::HillType::R94)], in, out) {}

	RedcodeParser::RedcodeParser(const std::shared_ptr<const std::map<std::string, int>> &Hill, std::istream &in, std::ostream &out)
			: d_scanner(in, out),
			  error_line_num(0),
			  instr_num(0),
			  expressions(1, new Expression(0)),
			  environments(1, Environment(Hill)) {}

	RedcodeParser::~RedcodeParser() {
		for (auto &expr : expressions)
			delete expr;
	}

	void RedcodeParser::assemble() {
		// solve all expressions
		solveAll(expressions, environments);
		auto expsIt = expressions.begin();
		auto envsIt = environments.begin();
		for (; expsIt != expressions.end(); ++expsIt, ++envsIt)
			if (!(*expsIt)->modSolve(*envsIt))
				(*expsIt)->getValue();  // throws exception with error description

		// create warrior
		if (tree.origin)
			warrior.origin = tree.origin->getValue();
		if (tree.pin)
			warrior.pin = tree.pin->getValue();
		warrior.comments = d_scanner.comments;
		warrior.reserve(instr_num);
		for (const auto &Node : tree.nodes)
			warrior.push_back(!Node.B
					? corewar::Instruction(Node.opcode, Node.A->getValue())
					: corewar::Instruction(Node.opcode, Node.A->getValue(), Node.B->getValue()));

		// AssertionError is recoverable, so it is thrown only when the warrior is ready
		for (const auto &Expr : tree.assertions)
			if (!Expr->getValue())
				throw corewar::AssertionError("assertion error");
	}

	inline void RedcodeParser::error() {
		throw corewar::AssemblerError(
				std::to_string(d_scanner.prevLineNr()) + ": syntax error");
	}

	inline int RedcodeParser::lex() {
		return d_scanner.lex();
	}

	inline void RedcodeParser::print() {
		print_();           // displays tokens if --print was specified
	}

	inline void RedcodeParser::exceptionHandler(std::exception const &exc) {
		throw corewar::AssemblerError(
				std::to_string(error_line_num > 0 ? error_line_num : d_scanner.prevLineNr())
				+ ": " + exc.what());
	}
}
