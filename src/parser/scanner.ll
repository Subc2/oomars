%class-header = "RedcodeScanner.hh"
%implementation-header = "RedcodeScanner.ihh"
%baseclass-header = "RedcodeScannerBase.hh"
%namespace = "parser"
%class-name = "RedcodeScanner"
%case-insensitive
Opcode    DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SEQ|SNE|SLT|SPL|NOP|LDP|STP
Modifier  [ABFXI]|AB|BA
Space     [ \t]
%%
{Space}+
\n|\r\n                             return '\n';
;redcode[^\n]*                      comments[CommentType::REDCODE] = matched().substr(8);
;name{Space}[^\n]*                  comments[CommentType::NAME] = matched().substr(6);
;author{Space}[^\n]*                comments[CommentType::AUTHOR] = matched().substr(8);
;version{Space}[^\n]*               comments[CommentType::VERSION] = matched().substr(9);
;date{Space}[^\n]*                  comments[CommentType::DATE] = matched().substr(6);
;strategy([^a-z0-9_\n][^\n]*)?      comments[CommentType::STRATEGY] += matched().substr(9 + !!isspace(matched()[9])) + "\n";
;kill{Space}[^\n]*                  comments[CommentType::KILL] = matched().substr(6);
;assert{Space}[^\n]*                {
                                      push(matched().substr(8));
                                      return RedcodeParser::ASSERT;
                                    }
;[^\n]*
[0-9]+|0x[0-9a-f]+                  return RedcodeParser::NUMBER;
{Opcode}(\.{Modifier})?/[^a-z0-9_]  return RedcodeParser::OPERATION;
EQU                                 return RedcodeParser::EQU;
FOR                                 return RedcodeParser::FOR;   // pMARS extension
ROF                                 return RedcodeParser::ROF;   // pMARS extension
ORG                                 return RedcodeParser::ORG;
END                                 return RedcodeParser::END;
PIN                                 return RedcodeParser::PIN;   // pMARS extension
!=                                  return RedcodeParser::NE;    // pMARS extension
"<="                                return RedcodeParser::LE;    // pMARS extension
==                                  return RedcodeParser::EQ;    // pMARS extension
">="                                return RedcodeParser::GE;    // pMARS extension
&&                                  return RedcodeParser::AND;   // pMARS extension
"||"                                return RedcodeParser::OR;    // pMARS extension
[a-z_][a-z0-9_]*                    try {
                                      pushEqu(equates.at(matched()));
                                    } catch (const std::out_of_range &) {
                                      return RedcodeParser::NAME;
                                    }
.                                   return matched()[0];
<<EOF>>                             return lnc.prev_token == '\n' ? EOF : '\n';
