# Documentation #

[Annotated Draft of the Proposed 1994 Core War Standard](icws94.txt) from
<http://corewar.co.uk/standards/icws94.txt>.

[pMARS 0.9.2 man page](pmars.6) extracted from
<https://sourceforge.net/projects/corewar/files/pMARS/0.9.2/pmars-0.9.2.tar.gz>.
See sections *REDCODE* and *P-SPACE*.

[The beginners' guide to Redcode](guide.html) from
<http://vyznev.net/corewar/guide.html>.
