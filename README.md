# Object-oriented Memory Array Redcode Simulator (OoMARS) #

![](screenshot.png)

## Requirements ##

Runtime dependencies:
- libgmp10
- libgmpxx4
- libqt5widgets5 (for Qt interface)
- libstdc++6

Build dependencies:
- CMake 3.7.0 or newer
- Bisonc++ 6.01.01 or newer
- Flexc++ 2.07.00 or newer
- GCC 4.9 or newer
- GMP 5.1.0 or newer
- Qt 5.0 or newer (when building Qt interface)

## Installation ##

Install required packages:
```sh
sudo apt install cmake bisonc++ flexc++ g++ libgmp-dev qt5-default
```

Compile application:
```sh
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
ln -rs oomars test ..
```

Run tests like this:
```sh
./fetch-archive.sh
./fix-archive.sh
time bash -c 'for FILE in archive/*.{txt,supported}; do ./test $FILE; done'
time bash -c 'for FILE in archive/*.{disabled,unsupported}; do ./test $FILE 2> /dev/null && echo $FILE; done'
```
